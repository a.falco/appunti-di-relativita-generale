\chapter[Richiami di relatività ristretta]{Richiami di relatività ristretta}
La relatività ristretta assume l'esistenza dei sistemi di riferimento inerziali in cui gli oggetti liberi si muovono di moto rettilineo uniforme. Un sistema di riferimento $O$ associa ad ogni evento una coordinata spaziale $\va{x}$ e una temporale $t$. Il \textit{principio di relatività} dice che se un sistema $O'$ si muove di moto uniforme con velocità $v$ rispetto ad $O$ allora è anch'esso inerziale e deve descrivere le leggi della fisica in maniera equivalente. Quindi le leggi devono essere \textit{covarianti} rispetto a trasformazioni di coordinate tra due sistemi inerziali.

Le trasformazioni di Galileo sono
\[\begin{cases}t'=t\\\va{x}'=\va{x}-\va{v}t\end{cases}\qquad\text{il tempo è assoluto}\]
Le velocità si sommano vettorialmente. Se $\va{u}$ è una velocità nel sistema $O$, la velocità vista da $O'$ sarà $\va{v}'=\va{u}-\va{v}$, non c'è una velocità $\va{w}$ assoluta. L'equazioni gravitazionali in meccanica newtoniana sono invarianti rispetto alle equazioni di Galileo.

L'elettrodinamica, ed in particolare la universalità della velocità della luce $c$, ha imposto una modifica. Il principio di relatività rimane valido, sono le trasformazioni a dovere essere modificate. La costanza di $c$ implica che se $c\Delta t=|\Delta\va{x}|$ in un sistema $O$ allora $c\Delta t'=|\Delta\va{x}'|$ in $O'$.
\[c^2\Delta t^2-\Delta\va{x}^2=0\Longrightarrow c^2\Delta t'^2-\Delta\va{x}'^2=0\]
Se imponiamo che le trasformazioni siano lineari, con considerazione di simmetria otteniamo
\[c^2\Delta t^2-\Delta\va{x}^2=c^2\Delta t'^2-\Delta\va{x}'^2\]
sempre, cioè non solo quando $c\Delta t = |\Delta\va{x}|$.
\[c^2\Delta t^2-\Delta\va{x}^2=\Delta s^2\]
è quindi un invariante di queste "nuove" trasformazioni. $\Delta s$ è l'intervallo fra due eventi (chiamato anche \textit{intervallo invariante}).


\section{Notazione quadrivettoriale}
Introduciamo ora la notazione quadrivettoriale.
\[x^\mu=(ct,\va{x})\qquad\Delta x^\mu=(c\Delta t,\Delta\va{x})\]
usiamo la notazione di vettore colonna
\[\left[x^\mu\right]=\begin{pmatrix}ct\\x^1\\x^2\\x^3\end{pmatrix}\]
la trasformazione lineare
\[x^\mu\longrightarrow x'^\mu=\Lambda^\mu_{\ \nu} x^\nu\]
\[\left[x'^\mu\right]=\left[\Lambda^\mu_{\ \nu}\right]\cdot\left[x^\nu\right]\qquad\text{dove}\qquad\left[\Lambda^\mu_{\ \nu}\right]=\begin{pmatrix}\Lambda^0_{\ 0}&\Lambda^0_{\ 1}&\Lambda^0_{\ 2}&\Lambda^0_{\ 3}\\\Lambda^1_{\ 0}&\Lambda^1_{\ 1}&\Lambda^1_{\ 2}&\Lambda^1_{\ 3}\\\Lambda^2_{\ 0}&\Lambda^2_{\ 1}&\Lambda^2_{\ 2}&\Lambda^2_{\ 3}\\\Lambda^3_{\ 0}&\Lambda^3_{\ 1}&\Lambda^3_{\ 2}&\Lambda^3_{\ 3}\end{pmatrix}\]
$x^\mu$ con l'indice in alto è un vettore \textit{controvariante}. Definiamo il vettore \textit{covariante} (o covettore)
\[x_\mu=\eta_{\mu\nu}x^\nu\qquad\left[\eta\right]=\begin{pmatrix}1&0&0&0\\0&-1&0&0\\0&0&-1&0\\0&0&0&-1\end{pmatrix}\]
la \textit{lunghezza invariante} è quindi\footnote{Con queste notazioni, gli indici ripetuti si sommano
    \["\eta_{\mu\nu}x^\nu"\equiv\sum_{\nu=0}^3\eta_{\mu\nu}x^\nu\]}
\[s^2=x_\mu x^\mu=x^\mu\eta_{\mu\nu}x^\nu\qquad\text{oppure}\qquad\left[x^\mu\right]^t\cdot\left[\eta_{\mu\nu}\right]\cdot\left[x^\nu\right]\]
per trovare $\Lambda^\mu_{\ \nu}$ dobbiamo quindi imporre
\[x'^\mu\eta_{\mu\nu}x'^\nu=x^\mu\eta_{\mu\nu}x^\nu\qquad\left[x'\right]^t\cdot\left[\eta\right]\cdot\left[x'\right]=\left[x\right]^t\cdot\left[\eta\right]\cdot\left[x\right]\]
\[\Lambda^\mu_{\ \rho} x^\rho\eta_{\mu\nu}\Lambda^\nu_{\ \tau} x^\tau=x^\mu\eta_{\mu\nu}x^\nu=x^\rho\eta_{\rho\tau}x^\tau\]
dove nell'ultima uguaglianza abbiamo usato il fatto che gli indici sommati sono muti\footnote{ovvero sono sommati e quindi possono essere rinominati}. Siccome deve valere
\[\eta_{\rho\tau}=\Lambda^\mu_{\ \rho}\Lambda^\nu_{\ \tau}\eta_{\mu\nu}\quad\forall x^\mu\]
con la notazione matriciale
\[\left[\Lambda\right]^t\cdot\left[\eta\right]\cdot\left[\Lambda\right]=\left[\eta\right]\]
Nota che in geometria euclidea $[\eta]$ è sostituito con la matrice identità $[\Lambda]^t\cdot[\Lambda]=\mathbb{I}$ e otteniamo le matrici ortogonali. Ora invece
\[[\eta]=\text{diag}(1,-1,-1,-1)=\text{diag}(1,-\mathbb{I}_{3\times3})\]


\section{Trasformazioni spaziali}
\subsection{Rotazioni e boost}
Per tutte le trasformazioni puramente spaziali, cioè $t'=t$, vale ancora $[\Lambda]^t\cdot[\Lambda]=\mathbb{I}$. Ad esempio la rotazione nel piano $x^1x^2$%{\tiny linea tratteggiata?}
\[[\Lambda]=\begin{pmatrix}1\ \ \vline&0&0&0\\\hline0\ \ \vline&\cos\alpha&-\sin\alpha&0\\0\ \ \vline&\sin\alpha&\cos\alpha&0\\0\ \ \vline&0&0&1\end{pmatrix}\]
Ci sono 3 rotazioni di questo tipo e formano il gruppo $SO(3)$. Altre 3 sono i \textit{boost}, cioè le trasformazioni in cui $O'$ si muove con velocità uniforme rispetto a $O$.
\[[\Lambda]=\begin{pmatrix}\cosh(\alpha)&\sinh(\alpha)&\vline\ \ 0&0\\\sinh(\alpha)&\cosh(\alpha)&\vline\ \ 0&0\\\hline0&0&\vline\ \ 1&0\\0&0&\vline\ \ 0&1\end{pmatrix}\]
Questo è il boost lungo la direzione $x^1$. Verifichiamo%{\tiny ci vorrebbe gather e non split}
\[[\Lambda]^t\cdot[\eta]\cdot[\Lambda]=[\eta]\]
\begin{gather*}
    \begin{pmatrix}\cosh\alpha&\sinh\alpha\\\sinh\alpha&\cosh\alpha\end{pmatrix}\begin{pmatrix}1&0\\0&-1\end{pmatrix}\begin{pmatrix}\cosh\alpha&\sinh\alpha\\\sinh\alpha&\cosh\alpha\end{pmatrix}=\\\begin{pmatrix}\cosh^2\alpha-\sinh^2\alpha=1&\cosh\alpha\sinh\alpha-\sinh\alpha\cosh\alpha=0\\\sinh\alpha\cosh\alpha-\cosh\alpha\sinh\alpha=0&\sinh^2\alpha-\cosh^2\alpha=-1\end{pmatrix}
\end{gather*}
$\alpha$ è chiamata \textit{rapidità} della trasformazione. Abbiamo quindi il gruppo delle trasformazioni di Lorentz, $SO(3,1)$, definito proprio da $[\Lambda]^t\cdot[\eta]\cdot[\Lambda]=[\eta]$, di dimensione $6$, costituito dalle $6$ trasformazioni sopra e tutte le loro composizioni. Per trovare la velocità in $O'$ corrispondente prendiamo una particella ferma in $O$, ad esempio $\begin{pmatrix}ct\\0\end{pmatrix}$ (ferma nell'origine di $O$)
\[ct'=\cosh(\alpha)ct\qquad x'=\sinh(\alpha)ct\]
Quindi
\[v=\frac{x'}{t'}=c\frac{\sinh(\alpha)}{\cosh(\alpha)}\qquad\alpha=\arctanh\left(\frac{v}{c}\right)\]
\[\begin{pmatrix}\cosh\alpha&\sinh\alpha\\\sinh\alpha&\cosh\alpha\end{pmatrix}=\gamma\begin{pmatrix}1&v/c\\v/c&1\end{pmatrix}\qquad\gamma=\frac{1}{\sqrt{1-\frac{v^2}{c^2}}}\]
% \[\begin{pmatrix}\cosh\alpha&\sinh\alpha\\\sinh\alpha&\cosh\alpha\end{pmatrix}=\begin{pmatrix}\frac{1}{\sqrt{1-\frac{v^2}{c^2}}}&\frac{v/c}{\sqrt{1-\frac{v^2}{c^2}}}\\\frac{v/c}{\sqrt{1-\frac{v^2}{c^2}}}&\frac{1}{\sqrt{1-\frac{v^2}{c^2}}}\end{pmatrix}\qquad\gamma=\frac{1}{\sqrt{1-\frac{v^2}{c^2}}}\]
Siccome $c^2t^2-\va{x}^2=c^2t'^2-\va{x}'^2$, la trasformazione ha come "orbite" le iperboli in $x$, $ct$.

\begin{center}
    \begin{tikzpicture}
        \draw[thick,->] (0,-2) -- (0,2) node[anchor=south east] {$ct$};
        \draw[thick,->] (-2,0) -- (2,0) node[anchor=north west] {$x$};
        \draw[gray,thick,->] (0.5,-2) -- (-0.5,2) node[anchor=south east] {$ct'$};
        \draw[gray,thick,->] (-2,0.5) -- (2,-0.5) node[anchor=north west] {$x'$};
        \draw[thick,dashed] (-2,-2) -- (2,2);
        \draw[thick,dashed] (2,-2) -- (-2,2);
    \end{tikzpicture}
    \hspace{25pt}
    \begin{tikzpicture}
        \begin{axis}[axis lines = center, xmin=-4,xmax=4, ymin=-4,ymax=4, xticklabels=\empty, yticklabels=\empty, xlabel={$x$}, ylabel={$ct$}]
            \addplot [black,thick,domain=-2:2,->] ({cosh(x)}, {sinh(x)});
            \addplot [black,thick, domain=-2:2,<-] ({-cosh(x)}, {sinh(x)});
            \addplot [black,thick,domain=-2:2,->] ({sinh(x)}, {cosh(x)});
            \addplot [black,thick, domain=-2:2,->] ({-sinh(x)}, {-cosh(x)});
            \addplot[gray,dashed,domain=-4:4,<->] expression {x};
            \addplot[gray,dashed,domain=-4:4,<->] expression {-x};
        \end{axis}
    \end{tikzpicture}
\end{center}

Nota importante: quando scriviamo $x'^\mu=\Lambda^\mu_{\ \nu}x^\nu$ intendiamo che un certo punto $p$ nello spazio-tempo $M$ viene descritto nel sistema $O$ dal quadrivettore $x'^\mu(p)$ e nel sistema $O'$ dal quadrivettore $x'^{\mu}(p)$. $p$ rimane sempre lo stesso.

\subsection{Cono luce}
Dato un evento $p$ che prendiamo in $(0,0)$, possiamo dividere lo spazio tempo in 3 zone separate dal \textit{cono luce}:
\begin{enumerate}
    \item il futuro assoluto di $p$ ($s^2>0$ e $t>0$),
    \item il passato assoluto ($s^2>0$ e $t<0$),
    \item la zona di possibile simultaneità ($s^2<0$).
\end{enumerate}
\begin{center}
    \begin{tikzpicture}
        % (0,1) node[anchor=south] {adhud}
        \fill[black] (0,1.2) circle (0pt) node[anchor=south] {Futuro ($p$)};
        \fill[black] (0,-1.2) circle (0pt) node[anchor=north] {Passato ($p$)};
        \fill[black] (0,0) circle (2pt) node[anchor=south west] {$p$};
        \draw[thick,->] (0,-2) -- (0,2) node[anchor=south east] {$ct$};
        \draw[thick,->] (-2,0) -- (2,0) node[anchor=north west] {$x$};
        \draw[thick,dashed] (-2,-2) -- (2,2);
        \draw[thick,dashed] (2,-2) -- (-2,2);
    \end{tikzpicture}
\end{center}

In relatività possiamo mantenere il principio di causalità se assumiamo che $c$ sia la velocità massima di ogni particella.

\begin{tikzpicture}
    % (0,1) node[anchor=south] {adhud}
    \draw[thick,->] (0,-2) -- (0,2) node[anchor=south east] {$ct$};
    \draw[thick,->] (-2,0) -- (2,0) node[anchor=north west] {$x$};
    \draw (1,-2) .. controls (0.3,-0.2) and (1.2,0.2) .. (1,2) node[anchor=west] {\parbox{2.3cm}{\textit{Linea universo} della particella}}; %controls (0.5,-0.5) and (1,0.5)
    \fill[black] (0.96,0.7) circle (2pt); %\fill[black] (0.89,0.7) circle (2pt);
    \fill[black] (0.74,-0.5) circle (2pt); %\fill[black] (0.78,-0.5) circle (2pt);
    \fill[black] (0.78,-1.3) circle (2pt); %\fill[black] (0.82,-1.3) circle (2pt);
\end{tikzpicture}

\begin{wrapfigure}{r}{0.5\textwidth}
    \begin{tikzpicture}[scale=0.6]
        \begin{axis}[axis lines=center,
            xlabel={$x$},
            ylabel={$y$},
            zlabel={$ct$},
            axis on top,
            set layers=default,
            xtick=\empty,
            ytick=\empty,
            ztick=\empty,
            xrange=-2:2,
            yrange=-2:2,
            unit vector ratio=1 1 1,% <- HERE (taken from Phelype Oleinik's deleted answer)
            scale=3 %<- added to compensate for the downscaling
            % resulting from unit vector ratio=1 1 1
            ]
            % plot
            \addplot3[domain=0:1,y domain=0:2*pi,colormap/viridis,surf]
            ({x*cos(deg(y))},{x*sin(deg(y))},{x});
            % \addplot3[domain=-1:1,y domain=-pi:0,colormap/viridis,surf,on layer=axis foreground] ({x*cos(deg(y))},{x*sin(deg(y))},{-x});
            % \addplot3[domain=0:1,y domain=-pi:pi,colormap/viridis,surf,on layer=axis foreground] ({x*cos(deg(y))},{x*sin(deg(y))},{-x});
            \addplot3[domain=0:1,y domain=0:2*pi,colormap/viridis,mesh]
            ({x*cos(deg(y))},{x*sin(deg(y))},{-x});
        \end{axis}
    \end{tikzpicture}
\end{wrapfigure}
\noindent
Ricordiamo che siamo in $3+1$ dimensioni quindi il cono è fatto così:
\begin{comment}
    \begin{tikzpicture}
        \draw[dashed] (0,0) arc (170:10:2cm and 0.4cm)coordinate[pos=0] (a);
        \draw (0,0) arc (-170:-10:2cm and 0.4cm)coordinate (b);
        \draw[densely dashed] ([yshift=4cm]$(a)!0.5!(b)$) -- node[right,font=\footnotesize] {$h$}coordinate[pos=0.95] (aa)($(a)!0.5!(b)$)
        -- node[above,font=\footnotesize] {$r$}coordinate[pos=0.1] (bb) (b);
        \draw (aa) -| (bb);
        \draw (a) -- ([yshift=4cm]$(a)!0.5!(b)$) -- (b);
    \end{tikzpicture}
\end{comment}
\begin{comment}
    \begin{tikzpicture}
        \begin{axis}[axis lines = center]
            \addplot3[surf]{sqrt(x^2+y^2)};
            \addplot3[surf]{-sqrt(x^2+y^2)};
        \end{axis}
    \end{tikzpicture}
\end{comment}

Se i vettori (controvarianti) trasformano
\[x^\mu\rightarrow x'^\mu=\Lambda^\mu_{\ \nu} x^\nu\]
i covettori (covarianti) devono trasformare come segue:
\[x'_\mu=\eta_{\mu\nu}x'^\nu=\eta_{\mu\nu}\Lambda^\nu_{\ \rho} x^\rho=\eta_{\mu\nu}\Lambda^\nu_{\ \rho}\eta^{\rho\tau}x_\tau\]\footnote{\[[\eta^{\mu\nu}]=[\eta_{\mu\nu}]^{-1}=\begin{pmatrix}1&0&0&0\\0&-1&0&0\\0&0&-1&0\\0&0&0&-1\\\end{pmatrix}=[\eta_{\mu\nu}]\]}
Quindi
\[x_\mu\rightarrow x'_\mu=\Lambda_\mu^{\ \nu} x_\nu\qquad\text{dove}\quad\Lambda_\mu^{\ \nu}\equiv\eta_{\mu\rho}\eta^{\nu\tau}\Lambda^\rho_{\ \tau}\]
$\Lambda^\mu_{\ \nu}$ e $\Lambda_\mu^{\ \nu}$ sono due matrici in genere diverse.

Rifacciamolo in forma matriciale
\[[x'^\mu]=[\Lambda^\mu_{\ \nu}]\cdot[x^\nu]\]
\[[x'_\mu]=[\eta_{\mu\nu}]\cdot[x'^\nu]=[\eta_{\mu\nu}]\cdot[\Lambda^\nu_{\ \rho}]\cdot[x^\rho]=[\eta_{\mu\nu}]\cdot[\Lambda^\nu_{\ \rho}]\cdot[\eta^{\rho\tau}]\cdot[x_\tau]=[\Lambda^{\ \tau}_\mu]\cdot[x_\tau]\]
dove
\[[\Lambda^{\ \tau}_\mu]=[\eta_{\mu\nu}]\cdot[\Lambda^\nu_{\ \rho}]\cdot[\eta^{\rho\tau}]=\eta\Lambda\eta\]
Quindi se il vettore trasforma con $\Lambda$, il covettore trasforma con $\eta\Lambda\eta={\Lambda^{-1}}^t$.

Dimostriamo che $\eta\Lambda\eta={\Lambda^{-1}}^t$: $\Lambda^t\eta\Lambda\eta=\mathbb{I}$ da cui $\Lambda^t\eta\Lambda=\eta$ che è la definizione di trasformazione di Lorentz.

Solo per le trasformazioni ortogonali ($\Lambda^t=\Lambda^{-1}$) i vettori e i covettori hanno la stessa trasformazione. Questo è il motivo per cui in geometria euclidea non è necessario fare la distinzione fra indici "sopra" e "sotto". In relatività è invece importante. Ancora di più lo sarà in relatività generale.

Lo spazio-tempo della relatività ristretta si chiama \textit{spazio di Minkowski} $\mathbb{R}^{3,1}$. $\eta_{\mu\nu}$ è la \textit{metrica} dello spazio di Minkowski perché definisce un intervallo invariante $\eta_{\mu\nu}x^\mu x^\nu$.

\subsection{Tensori}
I tensori sono ogetti lineari con vari indici. Un tensore $(r,s)$ è definito da $\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}$ sono $4^{r+s}$ numeri reali, un oggetto in $\mathbb{R}^{r+s}$. Il tensore vive in uno spazio vettoriale e trasforma nel seguente modo
\[\tensor{{T'}}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}=\Lambda^{\mu_1}_{\ \rho_1}\ldots\Lambda^{\mu_r}_{\ \rho_r}\Lambda^{\ \tau_1}_{\nu_1}\ldots\Lambda^{\ \tau_s}_{\nu_s}\tensor{T}{^{\rho_1\ldots\rho_r}_{\tau_1\ldots\tau_s}}\]
I vettori $A^\mu$ sono tensori $(1,0)$, i covettori $A_\mu$ sono tensori $(0,1)$. La metrica $\eta_{\mu\nu}$ è un tensore $(0,2)$, ma uno molto speciale che rimane invariante sotto trasformazioni. $\delta^\mu_{\ \nu}$, $[\delta^\mu_{\ \nu}]=\text{diag}(1,1,1,1)$ è un tensore $(1,1)$, anch'esso invariante. Un altro tensore invariante è $\epsilon^{\mu\nu\rho\gamma}$, il tensore $(4,0)$ completamente antisimmetrico negli indici con $\epsilon^{0123}=1$ e tutte le sue permutazioni sono $+1$ o $-1$. Tutte le componenti con due indici uguali sono nulle per antisimmetria.
\[\epsilon'^{\mu_1\mu_2\mu_3\mu_4}=\Lambda^{\mu_1}_{\ \nu_1}\Lambda^{\mu_2}_{\ \nu_2}\Lambda^{\mu_3}_{\ \nu_3}\Lambda^{\mu_4}_{\ \nu_4}\epsilon^{\nu_1\nu_2\nu_3\nu_4}\]
usando l'antisimmetria
\[\epsilon'^{\mu_1\mu_2\mu_3\mu_4}=\det\Lambda\,\epsilon^{\mu_1\mu_2\mu_3\mu_4}\]
$\det\Lambda=1$ quindi è un tensore invariante.

\section{Analisi in $\mathbb{R}^{3,1}$}
Ora ci servono alcune nozioni di analisi sullo spazio $\mathbb{R}^{3,1}$. Una funzione $f:\mathbb{R}^{3,1}\rightarrow\mathbb{R}$ associa un numero reale $f(x^\mu)$ ad ogni punto $x^\mu$. Analogamente possiamo avere funzioni tensoriali, o campi tensoriali, in un certo tensore $\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}(x^\mu)$. $\eta_{\mu\nu}$ ad esempio è una funzione tensoriale in cui ad ogni punto $x^\mu$ si associa una metrica $\eta_{\mu\nu}$, in questo caso costante.

\subsection{Derivate}
Le derivate mantengono la struttura tensoriale. Data $f(x_\mu)$
\[\frac{\partial f}{\partial x^\mu}\equiv A_\mu\]
è un covettore (un campo di vettori covarianti).
\[A_\mu\rightarrow A'_\mu=\frac{\partial f}{\partial x'^\mu}=\frac{\partial f}{\partial x^\nu}\frac{\partial x^\nu}{\partial x'^\mu}\]

Ricorda che $\left[\frac{\partial x^\nu}{\partial x'^\mu}\right]$ è la matrice inversa di $\left[\frac{\partial x'^\mu}{\partial x^\nu}\right]$ quindi $\left[\frac{\partial f}{\partial x^\nu}\right]$ trasforma con l'inversa trasposta di $\Lambda^\mu_{\ \nu}$, cioè proprio come un covettore. Quindi la derivata porta i campi vettoriali
\[(r,s)\rightarrow(r,s+1)\text{ ,}\qquad\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}\rightarrow\frac{\partial}{\partial x^\mu}\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}\]
è consistente con il fatto che $df$, il differenziale della funzione fra due punti $x^\mu$ e $x^\mu+dx^\mu$ deve essere uno scalare
\[df=\frac{\partial f}{\partial x^\mu}\,dx^\mu\]
infatti $dx^\mu$ è un vettore mentre $\frac{\partial f}{\partial x^\mu}$ è un covettore\footnote{e quindi si possono contrarre}.

Possiamo anche contrarre l'indice della derivata con uno del tensore. Ad esempio dato un campo vettoriale $A^\mu(x^\mu)$, $\frac{\partial}{\partial x^\nu}A^\mu$ è un tensore $(1,1)$ e $\frac{\partial A^\mu}{\partial x^\mu}$ è uno scalare. $\frac{\partial A^\mu}{\partial x^\mu}$ è la divergenza di $A^\mu$.

\subsection{Integrali}
Anche gli integrali si possono esprimere in forma covariante. Ad esempio l'integrale su tutto lo spaziotempo è
\[\int dx^0\,dx^1\,dx^2\,dx^3\,f(x^\mu)=\int d\Omega\,f(x^\mu)\]
dove $d\Omega$ è la forma volume e $f$ una funzione. $d\Omega$ è invariante sotto trasformazioni di Lorentz, proprio perché $\det\Lambda=1$. Quindi se $f$ è una funzione scalare, $\int d\Omega\,f(x^\mu)$ è invariante di Lorentz. Questo verrà usato per costruire teorie invarianti partendo da azioni.


\section{Meccanica relativistica}
Possiamo ora iniziare ad usare le notazioni matematiche introdotte. Cominciamo studiando la meccanica relativistica delle particelle puntiformi libere.

L'azione non relativistica è
\[\mathcal{S}=\int dt\,\frac{1}{2}mv^2\]
per una particella libera di massa $m$. Vogliamo quindi trovare un'azione che sia scalare di Lorentz (cioè invariante) e che nel limite $v/c\ll1$ riproduca quella sopra.

Prendiamo l'intervallo invariante
\[\mathcal{S}=\alpha\int ds=\alpha\int\sqrt{c^2\,dt^2-d\va{x}^2}\]
$\alpha$ è una costante di proporzionalità da determinare. Riscrivendolo come integrale temporale possiamo fare il paragone con il limite $v/c\ll1$
\[\begin{split}\mathcal{S}&=\alpha\int\dd{t}c\sqrt{1-\frac{v^2}{c^2}}=\alpha\int\dd{t}c\qty(1-\frac{1}{2}\frac{v^2}{c^2}+\ldots)=\\&=\int\dd{t}\qty(\alpha c-\frac{1}{2}\alpha\frac{v^2}{c}+\ldots) \qquad \text{con} \quad v=\abs{\dv{\va{x}}{t}}\end{split}\]
$\alpha c$ è una costante e non cambia l'equazioni del moto. Vogliamo quindi
\[-\frac{1}{2}\alpha\frac{v^2}{c^2}=\frac{1}{2}mv^2\implies\alpha=-mc\]
quindi
\begin{equation}
    \boxed{\mathcal{S}_\text{mat}=-mc\int \dd{s}}=-mc\int\sqrt{\eta_{\mu\nu}\,dx^\mu\,dx^\nu}
\end{equation}

Facciamo ora la variazione in forma covariante. La traiettoria nello spazio-tempo è determinata da una quaterna di funzioni
\[\lambda\rightarrow x^\mu(\lambda)\]
$\lambda$ è un parametro qualsiasi che sia monotono. L'azione è
\[\mathcal{S}=-mc\int\,d\lambda\sqrt{\eta_{\mu\nu}\frac{dx^\mu}{d\lambda}\frac{dx^\nu}{d\lambda}}\]
L'azione è invariante per riparametrizzazione $\lambda\rightarrow\lambda'(\lambda)$, infatti l'informazione fisica è contenuta nella curva, cioè nell'immagine della funzione $x^\mu(\lambda)$, non nel modo in cui è parametrizzata. Ci sono parametrizzazioni particolari, che semplificano l'equazione del moto. Nel caso specifico possiamo prendere $\lambda=s+\text{cost}$, cioè parametrizzare proprio con l'intervallo partendo da un punto arbitrario.
\[\mathcal{S}=-mc\int\,ds\,\sqrt{\eta_{\mu\nu}\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}}\]
\[\var\mathcal{S}=-mc\int\,ds\,\frac{1}{2}\frac{2\eta_{\mu\nu}\frac{dx^\mu}{ds}\var\frac{dx^\nu}{ds}}{\sqrt{\eta_{\mu\nu}\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}}}\]
\footnote{abbiamo usato la simmetria della metrica}Ora usiamo il fatto che abbiamo scelto una parametrizzazione speciale
\begin{equation}\label{1.5}
    \eta_{\mu\nu}\frac{dx^\mu}{ds}\frac{dx^\nu}{ds}=1
\end{equation}
\[\var\S=-mc\int\,ds\,\eta_{\mu\nu}\frac{dx^\mu}{ds}\var\frac{dx^\nu}{ds}\]
facciamo ora l'integrale per parti
\[\var\S=mc\int\,ds\eta_{\mu\nu}\frac{d}{ds}\left(\frac{dx^\mu}{ds}\right)\var x^\nu+\text{termine di bordo}\]
Siccome deve essere $\var\S=0$ per ogni variazione $\var x^\nu$, otteniamo l'equazione del moto:
\begin{equation}
    \frac{d}{ds}\left(\frac{dx^\mu}{ds}\right)=0\qquad\left(\frac{d^2x^\mu}{ds^2}=0\right)
\end{equation}
$\frac{dx^\mu}{ds}\equiv u^\mu$ è il quadri-vettore velocità. Abbiamo già visto nella \ref{1.5} che $u^\mu u_\mu=1$. Il modulo di $u^\mu$ è costante, sempre 1, non importa quanto è veloce la particella. L'equazione del moto $\frac{du^\mu}{ds}=0$ dice che $u^\mu$ è costante lungo la traiettoria della particella. In forma vettoriale
\[u^\mu=\left(\frac{c\,dt}{ds},\frac{d\va{x}}{ds}\right)\qquad ds=c\,dt\sqrt{1-\frac{v^2}{c^2}}=\frac{c\,dt}{\gamma}\]
\[u^\mu=\left(\frac{1}{\sqrt{1-\frac{v^2}{c^2}}},\frac{\va{v}/c}{\sqrt{1-\frac{v^2}{c^2}}}\right)=(\gamma, \gamma\va{v}/c)\]
$\frac{du^\mu}{ds}$ contiene tre equazioni non triviali ($u^\mu u_\mu$ è sempre costante) ed è equivalente a $\frac{d\va{v}}{dt}=0$, cioè al moto rettilineo uniforme.

Per la luce $ds=0$ quindi non ha senso parlare di $x^\mu(\lambda)$ con $\lambda=s$. L'equazione che possiamo scrivere è
\[\frac{d^2x^\mu}{d\lambda^2}=0\qquad\text{dove}\quad\lambda=\alpha t+\beta\]
con $\alpha$ e $\beta$ costanti. Per $\lambda=t$ abbiamo come soluzione $\va{v}=\text{cost.}$ e $|\va{v}|=c$ come condizione iniziale. $\lambda=\alpha t+\beta$ è detto \textit{parametro affine}.


\section{Esercizi}

\subsection{Trasfomazione delle velocità}
Una particella si muove con velocità $\va{w}$ nel sistema $O$ con coordinate $x^\mu$. Vogliamo calcolare la velocità nel sistema $O'$, $x'^\mu=\Lambda^\mu_{\ \nu}(-\va{v})x^\nu$ dove il sistema $O'$ si muove con velocità $-\va{v}$ (rispetto a $O$).

Se fossimo in meccanica newtoniana avremmo $\va{w}'=\va{w}+\va{v}$, ora ci aspettiamo modifiche ma comunque vogliamo ritrovare questa formula per $|\va{w}|\ll c$, $|\va{v}|\ll c$.

Se $\va{v}=v\hat{x}$ abbiamo
\[[\Lambda^\mu_{\ \nu}]=\begin{pmatrix}\cosh\alpha&\sinh\alpha&0&0\\\sinh\alpha&\cosh\alpha&0&0\\0&0&1&0\\0&0&0&1\end{pmatrix}\qquad\text{con}\quad v=c\tanh(\alpha)\]
\[w'^1=\frac{dx'^1}{dt'}=c\frac{\sinh(\alpha)c\,dt+\cosh(\alpha)\,dx^1}{\cosh(\alpha)c\,dt+\sinh(\alpha)\,dx^1}=\frac{\frac{dx^1}{dt}+c\tanh(\alpha)}{1+\frac{1}{c}\tanh(\alpha)\frac{dx^1}{dt}}=\frac{w^1+v}{1+\frac{vw^1}{c^2}}\]
\[w'^2=\frac{dx'^2}{dt'}=c\frac{dx^2}{\cosh(\alpha)c\,dt+\sinh(\alpha)\,dx^1}=\frac{\frac{1}{\cosh(\alpha)}\frac{dx^2}{dt}}{1+\frac{1}{c}\tanh(\alpha)\frac{dx^1}{dt}}=\frac{w^2/\gamma}{1+\frac{vw^1}{c^2}}\]
\[w'^3=\frac{dx'^3}{dt'}=c\frac{dx^3}{\cosh(\alpha)c\,dt+\sinh(\alpha)\,dx^1}=\frac{\frac{1}{\cosh(\alpha)}\frac{dx^3}{dt}}{1+\frac{1}{c}\tanh(\alpha)\frac{dx^1}{dt}}=\frac{w^3/\gamma}{1+\frac{vw^1}{c^2}}\]
per $|vw^1|\ll c^2$ otteniamo prorpio la somma vettoriale $\va{w}'=\va{w}+\va{v}+\ldots$. $|\va{w}'|$ è comunque sempre più piccola di $c$
\[\frac{|\va{w}'|^2}{c^2}=\frac{1}{c^2\left(1+\frac{vw^1}{c^2}\right)^2}\left(\frac{1}{\gamma^2}\left({w^2}^2+{w^3}^2\right)+{w^1}^2+2w^1v+v^2\right)\]
Definiamo l'angolo $\theta$
\[\tan\theta=\frac{w_\parallel}{w_\perp}=\frac{w^1}{\sqrt{{w^2}^2+{w^3}^2}}\]

\begin{center}
    \begin{tikzpicture}
        \fill[black] (1.5,1.25) circle (0pt) node[anchor=south west] {$\theta$};
        \draw[black,thick,dashed] (1.5,1) -- (1.5,2.5);
        \draw[thick,->] (0,-1.5) -- (0,2.5) node[anchor=south east] {$x^{2,3}$};
        \draw[thick,->] (-1.5,0) -- (2.5,0) node[anchor=north west] {$x^1$};
        \draw[gray,thick,->] (-1,-1.5) -- (2.5,2);
    \end{tikzpicture}
    \hspace{25pt}
    \begin{tikzpicture}
        \fill[black] (1.5,1.25) circle (0pt) node[anchor=south west] {$\theta'$};
        \draw[black,thick,dashed] (1.5,1) -- (1.5,2.5);
        \draw[thick,->] (0,-1.5) -- (0,2.5) node[anchor=south east] {$x'^{2,3}$};
        \draw[thick,->] (-1.5,0) -- (2.5,0) node[anchor=north west] {$x'^1$};
        \draw[gray,thick,->] (-1,-1.5) -- (2.5,2);
    \end{tikzpicture}
\end{center}

L'angolo nel sistema $O'$ è:
\[\begin{split}\tan\theta'&=\frac{w'_\parallel}{w'_\perp}=\frac{w'^1}{\sqrt{{w'^2}^2+{w'^3}^2}}=\frac{1}{\sqrt{1-\frac{v^2}{c^2}}}\frac{w^1+v}{\sqrt{{w^2}^2}+{w^3}^2}=\gamma\underbrace{\frac{w^1+v}{w^1}\tan\theta}_{\text{\parbox{5.5em}{formula non relativistica}}}\\&=\gamma\frac{w\sin\theta+v}{w\cos\theta}\end{split}\]

\subsection{Simmetrie di scambio}
Dato un tensore $(2,0)$ $C^{\mu\nu}$ possiamo decomporlo in una parte simmetrica e una antisimmetrica per scambio.
\[S^{\mu\nu}\equiv\frac{1}{2}\left(C^{\mu\nu}+C^{\nu\mu}\right)\qquad\text{per definizione}\quad S^{\mu\nu}=S^{\nu\mu}\]
\[A^{\mu\nu}\equiv\frac{1}{2}\left(C^{\mu\nu}-C^{\nu\mu}\right)\qquad\text{per definizione}\quad A^{\mu\nu}=-A^{\nu\mu}\]
Quindi
\[C^{\mu\nu}=S^{\mu\nu}+A^{\mu\nu}\]
La simmetria per scambio rimane invariata per trasformazioni
\[S'^{\mu\nu}=\Lambda^\mu_{\ \rho}\Lambda^\nu_{\ \tau}S^{\rho\tau}\]
Se $S$ è simmetrico allora lo è anche $S'$
\[S'^{\nu\mu}=\Lambda^\nu_{\ \rho}\Lambda^\mu_{\ \tau}S^{\rho\tau}\]
ma gli indici sommati possono essere rinominati, qundi scambiando $\rho\leftrightarrow\tau$
\[S'^{\mu\nu}=\Lambda^\nu_{\ \tau}\Lambda^\mu_{\ \rho}S^{\tau\rho}\]
ora usiamo la simmetria di $S$
\[S'^{\mu\nu}=\Lambda^\nu_{\ \tau}\Lambda^\mu_{\ \rho}S^{\rho\tau}=S'^{\mu\nu}\]
Allo stesso modo si dimostra che se $A^{\mu\nu}$ è antisimmetrico lo è anche $A'^{\mu\nu}$. Quindi $S$ ed $A$ sono sottospazi invarianti per trasformazioni.

\subsection{Dilatazione dei tempi}
Una particella che si muove a velocità $v$ misura un \textit{tempo proprio}
\[d\tau=\frac{ds}{c}=dt\sqrt{1-\frac{v^2}{c^2}}<dt\]
$\Delta t=\gamma\Delta\tau$ è chiamata \textit{dilatazione dei tempi}. Non c'è paradosso perché il $\Delta t$ è misurato in punti spaziali diversi. Se prendiamo una traiettoria che torna nello stesso punto abbiamo ancora
\[\Delta\tau=\frac{1}{c}\int\,ds\,=\int\,dt\,\sqrt{1-\frac{v^2}{c^2}}<\Delta t\]

\begin{wrapfigure}{l}{0.5\textwidth}
    \begin{tikzpicture}
        \draw[thick,->] (0,-1) -- (0,3) node[anchor=south east] {$ct$};
        \draw[thick,->] (-1,0) -- (3,0) node[anchor=north west] {$x$};
        \draw[thick] (1,0.5) -- (1,2.5);
        \draw (1,0.5) .. controls (1.5,1.5) .. (1,2.5);
        \draw (1,0.5) .. controls (2,1) and (2,2) .. (1,2.5);
        \fill[black] (1,0.5) circle (2pt); \fill[black] (1,2.5) circle (2pt);
    \end{tikzpicture}
\end{wrapfigure}

La traiettoria in cui $\va{x}$ è fermo è la soluzione dell'equazione del moto, cioè quella che minimizza $-mc\int\,ds$, qundi massimizza il tempo porprio. Questo porta al \textit{paradosso dei gemelli}. Ancora nessun paradosso in realtà, il punto è che il sistema della particella in movimento è necessariamente non inerziale, almeno in qualche tratto.
