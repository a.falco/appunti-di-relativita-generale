\chapter[Derivata covariante]{Derivata covariante}

Abbiamo visto un esempio di derivata che da una funzione scalare $f\rightarrow\partial_\mu f$ fornisce un covettore
\[\frac{\partial f}{\partial x'^\mu}=\frac{\partial x^\nu}{\partial x'^\mu}\frac{\partial f}{\partial x^\nu}\]
Vogliamo generalizzare l'operazione derivata anche ai tensori.


\section{Derivata tensoriale}
In relatività ristretta dato un tensore $(r,s)\xrightarrow{\partial}(r,s+1)$ otteniamo un tensore $(r,s+1)$ facendo la derivata
\[\partial_{\nu_{s+1}}T^{\mu_1...\mu_r}_{\ \ \ \ \ \ \nu_1...\nu_s}\equiv(\partial T)^{\mu_1...\mu_r}_{\ \ \ \ \ \ \nu_1...\nu_{s+1}}\]
Se però ammettiamo qualsiasi trasformazione di coordinate questa operazione non fornisce un tensore. Prendiamo ad esempio un vettore $A^\mu$ e facciamo la derivata $\partial_\nu A^\mu$ in un sistema $O'$:
\[\begin{split}\frac{\p A'^\mu}{\p x'^\nu}&=\frac{\p}{\p x'^\nu}\left(\frac{\p x'^\mu}{\p x^\rho}A^\rho\right)=\frac{\p}{\p x'^\nu}\frac{\p x'^\mu}{\p x^\rho}A^\rho+\frac{\p x'^\mu}{\p x^\rho}\frac{\p}{\p x'^\nu}A^\rho=\\&=\underbrace{\frac{\p x^\tau}{\p x'^\nu}\frac{\p^2x'^\mu}{\p x^\tau\p x^\rho}A^\rho}_{(1)}+\underbrace{\frac{\p x'^\mu}{\p x^\rho}\frac{\p x^\tau}{\p x'^\nu}\frac{\p A^\rho}{\p x^\tau}}_{(2)}\end{split}\]
$(1)$: questo è un pezzo in più; $(2)$: questo è proprio il pezzo che ci aspetteremmo se $\p_\nu A^\mu$ fosse un vero tensore $(1,1)$.

Quindi in generale la semplice operazione derivata non fornisce un tensore. In generale infatti $\frac{\p^2x'^\mu}{\p x^\tau\p x^\rho}\neq0$.

Per calcolare la derivata facciamo la differenza fra due vettori
\[\frac{\partial A^\mu}{\partial x^\nu}=\frac{A^\mu(x^\nu+dx^\nu)-A^\mu(x^\nu)}{dx^\nu}\]
ma $A^\mu(x^\nu+dx^\nu)$ e $A^\mu(x^\nu)$ sono due vettori che appartengono a diversi spazi vettoriali: uno vive in $T_{x^\nu+dx^\nu}(M)$, l'altro in $T_{x^\nu}(M)$. Questo rende l'operazione non ben definita dal punto di vista geometrico: i due spazi vettoriali sono diversi e trasformano in maniera diversa. Definire una derivata covariante è equivalente alla definizione di un trasporto di vettori da un punto ad un altro (questo si chiama \textit{trasporto parallelo}).


\subsection{Spazio di Minkowski e connessione}
Partiamo da un caso particolare. Consideriamo uno spazio-tempo di Minkowski, cioè in cui esistono sistemi di coordinate inerziali in cui la metrica è $g_{\mu\nu}=\eta_{\mu\nu}$ costante in tutto $M$. Prendiamo $x'^\mu$ uno di questi sistemi inerziali. Allora vogliamo che nel sistema $O'$ la nozione di derivata sia uguale a quella data in relatività ristretta. Chiamiamo l'operatore di derivata $D$ $D:A^\mu\rightarrow\tensor{(DA)}{^\mu_\nu}$, spesso si indica $D_\nu A^\mu$ per semplicità. Quindi conosciamo l'operatore nel sistema inerziale: se ora imponiamo che $D_\nu A^\mu$ trasformi come un tensore $(1,1)$, abbiamo una sua definizione in qualsiasi sistema di coordinate.
\[\begin{split}D_\nu A^\mu &= \frac{\partial x'^\gamma}{\partial x^\nu}\frac{\partial x^\mu}{\partial x'^\rho}\frac{\partial A'^\rho}{\partial x'^\gamma} \\& = \frac{\partial x'^\gamma}{\partial x^\nu}\frac{\partial x^\mu}{\partial x'^\rho}\frac{\p x^\tau}{\p x'^\gamma}\frac{\p}{\p x^\tau}\left(\frac{\p x'^\rho}{\p x^\xi}A^\xi\right) \\& = \frac{\p x^\mu}{\p x'^\rho}\frac{\p}{\p x^\nu}\left(\frac{\p x'^\rho}{\p x^\xi}A^\xi\right) \\& = \frac{\p A^\mu}{\p x^\nu}+\frac{\p x^\mu}{\p x'^\rho}\frac{\p^2x'^\rho}{\p x^\nu\p x^\xi}A^\xi\end{split}\]
Questo definisce l'operatore $D_\nu$ in un generico sistema di coordinate. Possiamo riscriverlo utilizzando la metrica, con un po' di passaggi. Definiamo
\[\boxed{\Gamma^\mu_{\nu\xi}\equiv\frac{\partial x^\mu}{\partial x'^\rho}\frac{\partial^2x'^\rho}{\partial x^\nu \partial x^\xi}}\]
$\Gamma^\mu_{\nu\xi}$ è la \textit{connessione} nel sistema $x^\mu$. Nota che $\Gamma^\mu_{\nu\xi}=\Gamma^\mu_{\xi\nu}$. quindi:
\begin{equation}
        \boxed{D_\nu A^\mu=\p_\nu A^\mu+\Gamma^\mu_{\nu\xi}A^\xi}
\end{equation}
Ora scriviamo la metrica (ricordando che $g'_{\rho\gamma}=\eta_{\rho\gamma}$)
\[g_{\mu\nu}=\frac{\p x'^\lambda}{\p x^\mu}\frac{\p x'^\gamma}{\p x^\nu}\eta_{\lambda\gamma}\]
\[g_{\tau\mu}\Gamma^\mu_{\nu\xi}=\frac{\p x'^\lambda}{\p x^\tau}\underline{\underline{\frac{\p x'^\gamma}{\p x^\mu}\eta_{\lambda\gamma}\frac{\p x^\mu}{\p x'^\rho}}}\frac{\p^2x'^\rho}{\p x^\nu\p x^\xi}=\underline{\underline{\eta_{\lambda\rho}}}\frac{\p x'^\lambda}{\p x^\tau}\frac{\p^2x'^\rho}{\p x^\nu\p x^\xi}\]
\[\p_\tau g_{\mu\nu}=\frac{\p}{\p x^\tau}\left(\frac{\p x'^\lambda}{\p x^\mu}\frac{\p x'^\gamma}{\p x^\nu}\right)\eta_{\lambda\gamma}=\frac{\p^2x'^\lambda}{\p x^\tau\p x^\mu}\frac{\p x'^\gamma}{\p x^\nu}\eta_{\lambda\gamma}+\frac{\p x'^\lambda}{\p x^\mu}\frac{\p^2x'^\gamma}{\p x^\tau\p x^\nu}\eta_{\lambda\gamma}\]
quindi possiamo scrivere $\p g$ come una combinazione di $g\Gamma$
\[\begin{cases}
\p_\tau g_{\mu\nu}=g_{\nu\lambda}\Gamma_{\tau\mu}^\lambda + g_{\mu\lambda}\Gamma_{\nu\tau}^\lambda\\
\p_\mu g_{\nu\tau}=g_{\tau\lambda}\Gamma_{\mu\nu}^\lambda + g_{\nu\lambda}\Gamma_{\tau\mu}^\lambda\\
\p_\nu g_{\tau\mu}=g_{\mu\lambda}\Gamma_{\nu\tau}^\lambda + g_{\tau\lambda}\Gamma_{\mu\nu}^\lambda
\end{cases}\qquad\substack{\text{scriviamo le 8}\\\text{permutazioni}\\\text{cicliche}}\]
sommando le prime due e sottraendo la terza
\[\p_\tau g_{\mu\nu}+\p_\mu g_{\nu\tau}-\p_\nu g_{\tau\mu}=2g_{\nu\lambda}\Gamma^\lambda_{\tau\mu}\]
abbiamo quindi ottenuto $\Gamma$ in funzione di $g$:
\begin{equation}
        \boxed{\Gamma_{\tau\mu}^\lambda=\frac{1}{2}g^{\lambda\nu}\qty\Big(\p_\tau g_{\mu\nu}+\p_\mu g_{\nu\tau}-\p_\nu g_{\tau\mu})}
\end{equation}
Nel sistema $O'$, $\Gamma'=0$ perché $\p g'=\p\eta=0$. L'assunzione che abbiamo fatto, cioè l'esistenza di un sistema $x'$ globalmente inerziale ($g'_{\mu\nu}=\eta_{\mu\nu}$ ovunque) può anche essere rilassata. Infatti l'unica cosa che ci basta è che esista un sistema in cui $D'_\mu A'^\nu=\p_\mu A^\nu$ e per questo ci basta che le derivate della metrica $\frac{\p g'_{\mu\nu}}{\p x'^\rho}=0$ si annullino in un determinato punto. Questo si chiama sistema \textit{localmente inerziale}. Non tutte le metriche ammettono un sistema globalmente inerziale, ma tutte ammettono sistema localmente inerziale.


\subsection{Gradi di libertà}
Facciamo per ora solo il conto dei gradi di libertà.
\begin{itemize}
        \item Una generica metrica attorno ad un punto si può espandere:
        \[g_{\mu\nu}\left(x^\rho+\dd{\xr}\right)=g_{\mu\nu}\left(x^\rho\right)+\frac{\p g_{\mu\nu}\left(x^\rho\right)}{\p x^\lambda}\dd{\xl}+\frac{1}{2}\frac{\p^2g_{\mu\nu}}{\p x^\lambda\p x^\tau}\left(x^\rho\right)\dd{\xl}\dd{\xt}+\ldots\]
        \begin{itemize}
                \item $g_{\mu\nu}\left(x^\rho\right)$ ha $\frac{5\cdot4}{2}=10$ gradi di libertà,
                \item $\p_\lambda g_{\mu\nu}$ ne ha $4\cdot10=40$,
                \item $\p_\lambda\p_\tau g_{\mu\nu}$ ne ha $10\cdot10=100$.
        \end{itemize}
        \item Un'arbitraria trasformazione di coordinate:
        \[x'^\mu\left(x^\rho+dx^\rho\right)=x'^\mu\left(x^\rho\right)+\frac{\p x'^\mu}{\p x^\lambda}\left(x^\rho\right)\,dx^\lambda+\frac{1}{2}\frac{\p^2x'^\mu}{\p x^\lambda\p x^\tau}\left(x^\rho\right)\,dx^\lambda\,dx^\tau+\ldots\]
        \begin{itemize}
                \item $\frac{\p x'^\mu}{\p x^\lambda}\left(x^\rho\right)$ è una generica matrice $4\times4$ con\footnote{ricordiamo che affinché la trasformazione sia invertibile il determinante della matrice di cambio di coordinate deve essere non nullo} $\det\neq0$, ha quindi 16 gradi di libertà. $10$ possono essere usati per portare $g_{\mu\nu}$ nella forma $g'_{\mu\nu}=\text{diag}(1,-1,-1,-1)=\eta_{\mu\nu}$, i $6$ restanti sono le trasformazioni di Lorentz che lasciano invariata $\eta_{\mu\nu}$.
                \item $\frac{\p^2x'^\mu}{\p x^\lambda\p x^\tau}\left(x^\rho\right)$ ha $10\cdot4=40$ gradi di libertà. Questi sono esattamente uguali a quelli di $\p_\lambda g_{\mu\nu}$. Vedremo infatti che è possibile scegliere le derivate seconde della trasformazione in modo da avere $\frac{\p g'_{\mu\nu}}{\p x'^\rho}=0$. Questo è il sistema localmente inerziale in cui $\Gamma'^\mu_{\nu\rho}=0$.
                \item Andando avanti nell'espansione $\p_\lambda\p_\tau\p_\gamma x'^\mu$ ha $(4+12+4)\cdot4=80$\footnote{Dato che le derivate commutano, possiamo pensare ai gradi di libertà associati alle tre derivate come le combinazioni con ripetizione di $4$ elementi di classe $3$, $\binom{4+3-1}{3}=\binom{6}{3}=20$. Moltiplicando per i $4$ gradi di libertà associati a $\xmp$, abbiamo in totale $20\cdot4=80$ gradi di libertà.}. Quindi vediamo che non ci sono abbastanza gradi di libertà per cancellare anche $\frac{\p^2g'_{\mu\nu}}{\p x'^\lambda\p x'^\gamma}$. Non tutte le metriche sono equivalenti a $\eta_{\mu\nu}$ globalmente.
        \end{itemize}
\end{itemize}


\section{Trasformazione di \texorpdfstring{$\Gamma^\mu_{\nu\rho}$}{}}
Dimostriamo ora che $\Gamma^\mu_{\nu\rho}$ non è un tensore studiando le sue proprietà di trasformazione.
\[D_\nu A^\mu=\frac{\p A^\mu}{\p x^\nu}+\Gamma^\mu_{\nu\lambda}A^\lambda\]
è un tensore quindi
\[D'_\nu A'^\mu=\frac{\partial x'^\mu}{\partial x^\rho}\frac{\partial x^\gamma}{\partial x'^\nu}D_\gamma A^\rho\]
ma allo stesso tempo deve valere
\[D'_\nu A'^\mu=\frac{\p A'^\mu}{\p x'^\nu}+\Gamma'^\mu_{\nu\lambda}A'^\lambda\]
dove $\Gamma'^\mu_{\nu\lambda}$ sono le componenti della connessione nel nuovo sistema di riferimento. Quindi
\[\frac{\p A'^\mu}{\p x'^\nu}+\Gamma'^\mu_{\nu\lambda}A'^\lambda=\frac{\p x'^\mu}{\p x^\rho}\frac{\p x^\gamma}{\p x'^\nu}\left(\frac{\p A^\rho}{\p x^\gamma}+\Gamma^\rho_{\gamma\tau}A^\tau\right)\]
Ora riscriviamo tutto in funzione di $A'^\mu$ usando $A^\mu=\frac{\p x^\mu}{\p x'^\lambda}A'^\lambda$
\[\Gamma'^\mu_{\nu\lambda}A'^\lambda=-\frac{\p A'^\mu}{\p x'^\nu}+\frac{\p x'^\mu}{\p x^\rho}\frac{\p x^\gamma}{\p x'^\nu}\left(\frac{\p}{\p x^\gamma}\left(\frac{\p x^\rho}{\p x'^\tau}A'^\tau\right)+\Gamma^\rho_{\gamma\tau}\frac{\p x^\tau}{\p x'^\xi}A'^\xi\right)\]
il prodotto con il primo termine in parentesi si può riscrivere come:
\[\underbrace{\frac{\p x'^\mu}{\p x^\rho}\frac{\p x^\gamma}{\p x'^\nu}\frac{\p x^\rho}{\p x'^\tau}\frac{\p A'^\tau}{\p x^\gamma}}_{\pdv*{A'^\mu}{\xnp}}+\frac{\p x'^\mu}{\p x^\rho}\frac{\p^2x^\rho}{\p x'^\nu\p x'^\tau}A'^\tau\]
quindi abbiamo:
\[\Gamma'^\mu_{\nu\lambda}A'^\lambda=\frac{\p x'^\mu}{\p x^\rho}\frac{\p^2x^\rho}{\p x'^\nu\p x'^\tau}A'^\tau+\frac{\p x'^\mu}{\p x^\rho}\frac{\p x^\gamma}{\p x'^\nu}\Gamma^\rho_{\gamma\tau}\frac{\p x^\tau}{\p x'^\xi}A'^\xi\]
rinominando $\tau\ra\lambda$ e $\xi\ra\lambda$, otteniamo:
\[\Gamma'^\mu_{\nu\lambda}=\frac{\p x'^\mu}{\p x^\rho}\frac{\p x^\gamma}{\p x'^\nu}\frac{\p x^\tau}{\p x'^\lambda}\Gamma^\rho_{\gamma\tau}+\frac{\p x'^\mu}{\p x^\rho}\frac{\p^2x^\rho}{\p x'^\nu\p x'^\lambda}\]
il primo termine è lo stesso che avremmo se $\Gamma^\mu_{\nu\rho}$ fosse un tensore $(1,2)$; ma non lo è a causa del secondo termine.


\subsection{Sistema localmente inerziale}
Vediamo ora che è sempre possibile scegliere un sistema di coordinate in cui la connessione si annulla localmente. Prendiamo un sistema $O$ con coordinate $x^\mu$, $g_{\mu\nu}$ arbitraria e
\[\Gamma_{\nu\rho}^\mu=\frac{1}{2}g^{\mu\lambda}\qty\Big(\p_\nu g_{\lambda\rho}+\p_\rho g_{\lambda\nu}-\p_\lambda g_{\nu\rho})\]
Scriviamo ora la relazione fra $\Gamma$ e $\Gamma'$:
\[\Gamma^\mu_{\nu\rho}=\frac{\p x^\mu}{\p x'^\lambda}\frac{\p x'^\gamma}{\p x^\nu}\frac{\p x'^\tau}{\p x^\rho}\Gamma'^\lambda_{\gamma\tau}+\frac{\p x^\mu}{\p x'^\lambda}\frac{\p^2x'^\lambda}{\p x^\nu\p x^\rho}\]
Se vogliamo $\Gamma'=0$ dobbiamo scegliere le coordinate $x'^\mu$ in modo che
\[\Gamma^\mu_{\nu\rho}=\pdv{\xm}{\xlp}\pdv{\xlp}{\xn}{\xr}\]
Quindi basta scegliere una trasformazione
\[\xmp(\xr+\dd{\xr})=\xmp(\xr)+\pdv{\xmp}{\xl} (\xr)\dd{\xl}+\pdv{\xmp}{\xl}{\xt} (\xr)\dd{\xl}\dd{\xt}+\ldots\]
con le derivate seconde date da
\[\pdv{\xmp}{\xl}{\xt} (\xr)=\pdv{\xmp}{\xr}\Gamma^\rho_{\lambda\tau}\]
la scelta di $x'^\mu\left(x^\rho\right)$ e $\frac{\p x'^\mu}{\p x^\lambda}\left(x^\rho\right)$ può essere qualsiasi. Così è possibile anche avere $g'_{\mu\nu}=\eta_{\mu\nu}+\order{{\dd{\xrp}}^2}$, quindi $\pdv{\xrp}g'_{\mu\nu}=0$ e $\Gamma'^\mu_{\nu\rho}=0$. Abbiamo quindi dimostrato che in effetti i $40$ coefficienti di $\pdv{\xmp}{\xr}{\xg}$ possono essere scelti, in un solo modo, per annullare i 40 della derivata della metrica $\frac{\p g'_{\mu\nu}}{\p x'^\rho}$. Questo sistema è quello inerziale in cui la metrica è il "più vicino possibile" a $\eta_{\mu\nu}$. Questo è il sistema in cui si applica il principio di equivalenza: le leggi della fisica si riducono a quelle della relatività ristretta nel sistema localmente inerziale. La derivata covariante che abbiamo studiato è quella che diventa semplice derivata nel sistema localmente inerziale.


\section{Derivate di tensori}
Studiamo la derivata per tensori generici.

\subsection{Covettori}
La derivata covariante di un covettore $B_\mu$ si può ottenere usando la proprietà che $B_\nu A^\nu$ è uno scalare. Quindi
\[D_\mu\qty(B_\nu A^\nu)=\p_\mu\qty(B_\nu A^\nu)=\qty(\p_\mu B_\nu)A^\nu+B_\nu\qty(\p_\mu A^\nu)\]
ma deve valere anche
\[D_\mu\qty(B_\nu A^\nu)=\qty(D_\mu B_\nu)A^\nu+B_\nu\qty(\p_\mu A^\nu+\Gamma^\nu_{\mu\rho}A^\rho)\]
uguagliando otteniamo
\[\qty(D_\mu B_\nu)A^\nu=\qty(\p_\mu B_\nu)A^\nu-\Gamma^\nu_{\mu\rho}B_\nu A^\rho\]
rinominando $\nu\ra\lambda$ e $\rho\ra\nu$ nel secondo termine a destra dell'uguale otteniamo:
\begin{equation}
    \boxed{D_\mu B_\nu=\p_\mu B_\nu-\Gamma^\lambda_{\mu\nu}B_\lambda}
\end{equation}


\subsection{Tensori generici}
Usando lo stesso procedimento induttivamente possiamo trovare la derivata covariante per un tensore generico $(r,s)$
\[\begin{split}
        &D_\rho\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}=\\&\hspace{30pt}=\p_\rho \tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}+\sum_{i=1}^r\Gamma^{\mu_i}_{\rho\lambda_i}\tensor{T}{^{\mu_1\ldots\lambda_i\ldots\mu_r}_{\nu_1\ldots\nu_s}}-\sum_{j=1}^s\Gamma^{\lambda_j}_{\rho\nu_j}\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\lambda_j\ldots\nu_s}}
    \end{split}\]
% \[\resizebox{\hsize}{!}{$D_\rho\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}=\p_\rho \tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}+\sum_{i=1}^r\Gamma^{\mu_i}_{\rho\lambda_i}\tensor{T}{^{\mu_1\ldots\lambda_i\ldots\mu_r}_{\nu_1\ldots\nu_s}}-\sum_{j=1}^s\Gamma^{\lambda_j}_{\rho\nu_j}\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\lambda_j\ldots\nu_s}}$}\]
dove $D_\rho\tensor{T}{^{\mu_1\ldots\mu_r}_{\nu_1\ldots\nu_s}}$ è un tensore di tipo $(r,s+1)$. La derivata covariante permette di definire una nozione di \textit{trasporto parallelo}, cioè come spostare un vettore (o un tensore) lungo una curva.


\section{Trasporto parallelo}
Prendiamo una curva $x^\mu(\lambda):\mathbb{R}\ra M$. Un vettore $A^\mu(x^\nu)$ è trasportato parallelamente a se stesso lungo la linea se
\[\frac{dx^\mu}{d\lambda}D_\mu A^\nu=0\]
cioè se la derivata covariante proiettata lungo la tangente alla curva è nulla. Nota che l'equazione non dipende dalla parametrizzazione della curva ma solo dalla traiettoria, cioè l'immagine, nello spazio-tempo. Infatti, cambiando parametrizzazione abbiamo $\lambda\ra\lambda'(\lambda)$ e
\[\frac{d\lambda'}{d\lambda}\frac{d x^\mu}{d\lambda'}D_\mu A^\nu=0\implies\frac{dx^\mu}{d\lambda'}D_\mu A^\nu=0\]
Esplicitando $D_\mu$:
\[\dv{\xm}{\lambda}\qty\big(\p_\mu A^\nu+\Gamma^\nu_{\mu\rho}A^\rho)=0\]
scrivendo $A^\mu\left(x^\nu(\lambda)\right)$ come funzione di $\lambda$
\[\dv{A^\nu}{\lambda}+\dv{\xm}{\lambda}\Gamma^\nu_{\mu\rho}A^\rho=0\]
questa è un'equazione di primo grado, quindi per avere una soluzione basta specificare $A^\nu(\lambda_0)$, cioè il vettore in un dato punto della curva.

In un sistema localmente inerziale l'equazione diventa
\[\dv{A^\mu}{\lambda}=0\implies A^\mu\qty(\lambda)=\text{costante}\]
questa è infatti la nozione di trasporto parallelo in relatività ristretta: le componenti del vettore rimangono le stesse.


\subsection{Divergenza covariante}
Possiamo scrivere delle formule per la divergenza covariante. Prendiamo
\[D_\mu A^\mu=\p_\mu A^\mu+\Gamma^\mu_{\mu\nu}A^\nu\]
\[\Gamma^\mu_{\mu\nu}=\frac{1}{2}g^{\mu\rho}\qty\Big(\p_\mu g_{\rho\nu}+\p_\nu g_{\mu\rho}-\p_\rho g_{\mu\nu})\]
Se chiamiano $g=\det\left[g_{\mu\nu}\right]$, allora
\[\p_{\mu}g=gg^{\rho\gamma}\p_\mu g_{\rho\gamma}=-gg_{\rho\gamma}\p_\mu g^{\rho\gamma}\]
abbiamo usato la proprietà che che $gg^{\rho\gamma}$ è la matrice dei "minori". Inoltre $g^{\rho\gamma}g_{\rho\gamma}=\tr\mathbb{I}=4$ quindi
\[\p_\mu\qty(g^{\rho\gamma}g_{\rho\gamma})=g^{\rho\gamma}\p_\mu g_{\rho\gamma}+g_{\rho\gamma}\p_\mu g^{\rho\gamma}=0\]
quindi
\[\Gamma^\mu_{\mu\nu}=\frac{1}{2}\qty\Big(g^{\mu\rho}\p_\mu g_{\rho\nu}-g^{\mu\rho}\p_\rho g_{\mu\nu}+g^{\mu\rho}\p_\nu g_{\mu\rho})\]
notiamo che rinominando $\mu\lra\rho$ in uno dei primi due termini in parentesi questi si cancellano
\[\Gamma^\mu_{\mu\nu}=\frac{\p_\nu g}{2g}\qquad\text{da cui}\qquad D_\mu A^\mu=\p_\mu A^\mu+\frac{\p_\nu g}{2g}A^\nu\]
si può anche riscrivere come
\begin{equation}\label{6.3}
    \boxed{D_\mu A^\mu=\frac{1}{\sqrt{-g}}\p_\mu\qty(\sqrt{-g}A^\mu)}=\p_\mu A^\mu+\frac{1}{2}\frac{-\p_\mu g}{\abs{g}}A^\mu
\end{equation}


\subsection{Laplaciano covariante}
Possiamo anche scrivere la generalizzazione del laplaciano. Dato uno scalare $f$, $\p_\mu f$ è un covettore. Possiamo quindi definire un operatore che porta $f$ in un nuovo scalare:
\[\Delta f\equiv D_\mu\qty\big(g^{\mu\nu}\p_\nu f)=\frac{1}{\sqrt{-g}}\p_\mu\qty(\sqrt{-g}\,g^{\mu\nu}\p_\nu f)\]
Questo è l'operatore di \textit{Laplace-Beltrami}. Si riconduce al laplaciano nel sistema localmente inerziale.


\subsection{Teorema della divergenza}
Il teorema della divergeza si può riscrivere in forma covariante. L'integrale della divergenza è
\[\int\sqrt{-g}\dd{\Omega}D_\mu A^\mu=\int\dd{\Omega}\p_\mu\qty(\sqrt{-g}A^\mu)=\int\sqrt{-g}\dd{S_\mu}A^\mu\]
$\sqrt{-g}\dd{\Omega}$ e $D_\mu A^\mu$ sono entrambi scalari. La forma finale è anch'essa uno scalare: $A^\mu$ è un vettore e $\sqrt{-g}\,dS_\mu$ è il covettore ortogonale alla $3$-superficie.


\section{Esercizi}

\subsection{Coordinate sferiche}

\subsubsection{Derivata covariante}
Usiamo le formule di derivata covariante per il caso particolare di spazio di Minkowsky in coordinate sferiche.
\[\begin{split}&x^\mu=(ct,r,\theta,\vp)\\&ds^2=c^2dt^2-dr^2-r^2\left(d\theta^2+\sin^2\!\theta\,d\vp^2\right)\end{split}\qquad\left[g_{\mu\nu}\right]=\begin{pmatrix}1&0&0&0\\0&-1&0&0\\0&0&-r^2&0\\0&0&0&-r^2\sin^2\theta\end{pmatrix}\]
Calcoliamo la connessione
\[\Gamma_{\mu\nu\rho}\equiv g_{\mu\lambda}\Gamma^\lambda_{\ \nu\rho}=\frac{1}{2}\qty\Big(\p_\nu g_{\mu\rho}+\p_\rho g_{\mu\nu}-\p_\mu g_{\nu\rho})\]
le uniche componenti non nulle sono
\[\begin{split}&\Gamma_{r\theta\theta}=-\Gamma_{\theta r\theta}=-\Gamma_{\theta\theta r}=-\frac{1}{2}\p_r g_{\theta\theta}=r\\&\Gamma_{r\vp\vp}=-\Gamma_{\vp r\vp}=-\Gamma_{\vp\vp r}=-\frac{1}{2}\p_r g_{\vp\vp}=r\sin^2\theta\\&\Gamma_{\theta\vp\vp}=-\Gamma_{\vp\theta\vp}=-\Gamma_{\vp\vp\theta}=-\frac{1}{2}\p_\theta g_{\vp\vp}=r^2\sin\theta\cos\theta\end{split}\]
verifichiamo $\Gamma^\mu_{\mu\nu}=\frac{\p_\nu g}{2g}$
\[g=-r^4\sin^2\theta\]
\[\begin{split}\frac{\p_r g}{2g}&=\frac{-4r^3\sin^2\theta}{-2r^4\sin^2\theta}=\frac{2}{r}=\Gamma^\theta_{\theta r}+\Gamma^\vp{\vp r}=g^{\theta\theta}\Gamma_{\theta\theta r}+g^{\vp\vp}\Gamma_{\vp\vp r}=\\&=-\frac{1}{r^2}\qty(-r)-\frac{1}{r^2\sin^2\theta}\qty(-r\sin^2\theta)\qquad\text{ok}\end{split}\]
\[\begin{split}\frac{\p_\theta g}{2g}&=\frac{-2r^4\sin\theta\cos\theta}{-2r^4\sin^2\theta}=\frac{1}{\tan\theta}=\Gamma^\vp{\vp\theta}=g^{\vp\vp}\Gamma_{\vp\vp\theta}=\\&=-\frac{1}{r^2\sin^2\theta}\qty(-r^2\sin\theta\cos\theta)\qquad\text{ok}\end{split}\]

\subsubsection{Operatore di Laplace-Beltrami}
Calcoliamo l'operatore di Laplace-Beltrami in coordinate sferiche
\[\begin{split}\frac{1}{\sqrt{-g}}\p_\mu&\qty(\sqrt{-g}\,g^{\mu\nu}\p_\nu f)=\frac{1}{r^2\sin\theta}\Bigg(\p_{x^0}\qty(r^2\sin\theta\p_{x_0}f)+\p_r\qty(r^2\sin\theta(-1)\p_r f)+\\&
        +\p_\theta\qty(r^2\sin\theta\qty(-\frac{1}{r^2})\p_\theta f)+\p_\vp\qty(r^2\sin\theta\qty(-\frac{1}{r^2\sin^2\theta})\p_\vp f)\Bigg)=\\&
        =\frac{1}{c^2}\p_t^2f-\frac{1}{r^2}\p_r\qty(r^2\p_rf)-\frac{1}{r^2\sin\theta}\p_\theta\qty(\sin\theta\p_\theta f)-\frac{1}{r^2\sin^2\theta}\p_\vp^2f=\\&
        =\frac{1}{c^2}\p_t^2f-\Delta_3f\end{split}\]
dove $\Delta f$ è proprio il laplaciano tridimensionale in coordinate sferiche.


\subsection{Trasporto parallelo}
\begin{wrapfigure}{l}{0.4\textwidth}
    % \vspace{-19pt}
    \tdplotsetmaincoords{70}{110}
    \tdplotsetrotatedcoords{0}{0}{0}
    \begin{tikzpicture}[tdplot_main_coords,scale=0.6]
        \begin{scope}[tdplot_rotated_coords]
            \draw[thick,->] (-3.8,0,0) -- (5,0,0) node[anchor=north east]{$x_1$};
            \draw[thick,->] (0,-3.4,0) -- (0,4,0) node[anchor=north west]{$x_2$};
            \draw[thick,->] (0,0,-1.8) -- (0,0,3) node[anchor=north west]{$x_3$};
            % \draw[->,red] (0,0,0) -- (4,4,4);
            \path [fill] (0,0,0) coordinate (c) circle (1pt);
            \tdplotdrawarc[thick, tdplot_rotated_coords,->]{(c)}{3}{30}{390}{}{};
        \end{scope}
    \end{tikzpicture}
\end{wrapfigure}
% \noindent

\begin{comment}
    \tdplotsetmaincoords{70}{110}
    \tdplotsetrotatedcoords{0}{0}{0}
    \begin{tikzpicture}[tdplot_main_coords,scale=0.6]
        \begin{scope}[tdplot_rotated_coords]
            \draw[thick,->] (-3.8,0,0) -- (4.5,0,0) node[anchor=north east]{$x$};
            \draw[thick,->] (0,-3.4,0) -- (0,4,0) node[anchor=north west]{$y$};
            \draw[thick,->] (0,0,-1.8) -- (0,0,3) node[anchor=north west]{$z$};
            % \draw[->,red] (0,0,0) -- (4,4,4);
            \path [fill] (0,0,0) coordinate (c) circle (1pt);
            \tdplotdrawarc[thick, tdplot_rotated_coords,->]{(c)}{3}{30}{390}{}{};
        \end{scope}
    \end{tikzpicture}
\end{comment}

Studiamo un esempio di trasporto parallelo. Prendiamo
\[\vp\ra\left(tc, r=\text{cost.}, \theta=\frac{\pi}{2}, \vp\right)\]
questo è un cerchio di raggio fissato nel piano $x_3=0$. Ora scriviamo l'equazione del trasporto parallelo lungo questa curva. $\dv{A^\mu}{\lambda}+\dv{\xn}{\lambda}\Gamma^\mu_{\nu\rho}A^\rho=0$ ($\lambda=\vp$).
\[\begin{dcases}\frac{dA^t}{d\vp}=0\\\frac{dA^r}{d\vp}+\Gamma^r_{\vp\vp}A^\vp=0\\\frac{dA^\theta}{d\vp}+\Gamma^\theta_{\vp\vp}A^\vp=0\\\frac{dA^\vp}{d\vp}+\Gamma^\vp{\vp\theta}A^\theta+\Gamma^\vp{\vp r}A^r=0\end{dcases}\xRightarrow{(\theta=\frac{\pi}{2})}\begin{dcases}\frac{dA^t}{d\vp}=0\\\frac{dA^r}{d\vp}=rA^\vp\\\frac{dA^\theta}{d\vp}=0\\\frac{dA^\vp}{d\vp}=-\frac{1}{r}A^r\end{dcases}\]

\begin{wrapfigure}{r}{0.3\textwidth}
    \vspace{-19pt}
    \begin{tikzpicture}[scale=0.65]
        \def\r{1.8}; \def\a{60};
        \draw[thick,->] (-2.6,0) -- (2.8,0) node[anchor=north] {$x^1$};
        \draw[thick,->] (0,-2.6) -- (0,2.8) node[anchor= east] {$x^2$};
        \draw (0,0) circle (2);
        % freccie
        \foreach \x in {0,...,9}
        \draw [thick,red,->] ({2*cos(360*\x/10)},{2*sin(360*\x/10)}) -- ({2*cos(360*\x/10)+\r*cos(\a)},{2*sin(360*\x/10)+\r*sin(\a)});
    \end{tikzpicture}
\end{wrapfigure}
\noindent
Quindi $A^r$ e $A^\vp$ ruotano fra di loro. Infatti nel sistema cartesiano $A^1$ e $A^2$ rimangono costanti.

\begin{comment}
    \begin{center}
        \begin{tikzpicture}[scale=0.8]
            \def\r{1.8}; \def\a{60};
            \draw[thick,->] (-2.6,0) -- (2.8,0) node[anchor=north] {$x^1$};
            \draw[thick,->] (0,-2.6) -- (0,2.8) node[anchor= east] {$x^2$};
            \draw (0,0) circle (2);
            % freccie
            \foreach \x in {0,...,9}
            \draw [thick,red,->] ({2*cos(360*\x/10)},{2*sin(360*\x/10)}) -- ({2*cos(360*\x/10)+\r*cos(\a)},{2*sin(360*\x/10)+\r*sin(\a)});
        \end{tikzpicture}
    \end{center}
\end{comment}
Nota che nelle convenzioni "vecchie" si usa una normalizzazione diversa. Di solito si usa indicare
\[\va{A}=\At^r\vu{r}+\At\vu*{\theta}+\At\vu*{\vp}\]
dove $\vu{r}$, $\vu*{\theta}$, $\vu*{\vp}$ sono versori unitari. Nelle nuove covenzioni
\[A^r=\frac{1}{\sqrt{-g_{rr}}}\At^r=\At^r\quad A^\theta=\frac{1}{\sqrt{-g_{\theta\theta}}}\At^\theta=\frac{1}{r}\At^\theta\quad A^\vp=\frac{1}{\sqrt{-g_{\vp\vp}}}\At^\vp=\frac{1}{r\sin\theta}\At^\vp\]

Ad esempio scriviamo l'operatore divergenza
\[D_\mu A^\mu=\frac{1}{\sqrt{-g}}\p_\mu\qty(\sqrt{-g}\,A^\mu)=\p_tA^t+\frac{1}{r^2}\p_r\qty(r^2A^r)+\frac{1}{\sin\theta}\p_\theta\qty(\sin\theta A^\theta)+\p_\vp A^\vp\]
Questo è proprio l'operatore divergenza nelle coordinate sferiche e nelle "vecchie" convenzioni.


\subsection{Differenziale di \texorpdfstring{$g=\det\qty[g_{\mu\nu}]$}{}}
Dimostriamo che $\dd{g}=gg^{\mu\nu}\dd{g_{\mu\nu}}$. Possiamo diagonalizzare
\[\qty[g_{\mu\nu}]=\mqty(\dmat{g_{11}&&0\\&\ddots&\\0&&g_{NN}})\]
$\dd{g_{\mu\nu}}$ non è detto che sia diagonale, ma la prim'ordine in $\dd{g}$ solo i termini diagonali contano se $g_{\mu\nu}$ è già diagonale. Quindi abbiamo che $g=\prod_{\mu=1}^Ng_{\mu\mu}$ e $gg^{\mu\nu}\dd{g_{\mu\nu}}=\qty\big(\prod g_{\mu\mu})$.
\[\sum_{\nu=1}^N\qty(\frac{1}{g_{\nu\nu}}\dd{g_{\nu\nu}})=\sum_{\nu=1}^Ng_{11}\ldots\dd{g_{\nu\nu}}\ldots g_{NN}\]

Contiamo i gradi di libertà di $g_{\mu\nu}=\qty(\xr+\dd{\xr})$. $\xmp\qty(\xr+\dd{\xr})$ in varie dimensioni

\begin{center}
        \begin{table}[H]
                \begin{tabular}{c|c|c|c|c|c|c}
                        $d$ & $g_{\mu\nu}$ & $\p_\mu\xnp$ & $\p_\nu g_{\nu\rho}$ & $\p_\mu\p_\mu\xr$ & $\p_\mu\p_\nu g_{\rho\gamma}$ & $\p_\mu\p_\nu\p_\rho\xg$ \\
                        \hline
                        $1$ & $1$ & $1$ & $1$ & $1$ & $1$ & $1$ \\
                        $2$ & $3$ & $4$ & $6$ & $6$ & $9$ & $8$ \\
                        $3$ & $6$ & $9$ & $18$ & $18$ & $36$ & $30$ \\
                        $4$ & $10$ & $16$ & $40$ & $40$ & $100$ & $80$ \\
                        $N$ & $\frac{N(N+1)}{2}$ & $N^2$ & $\frac{N^2(N+1)}{2}$ & $\frac{N^2(N+1)}{2}$ & $\frac{N^2\qty(N+1)^2}{4}$ & $\frac{N^2(N+1)(N+2)}{6}$ \\
                        \multicolumn{1}{c}{} &
                        \multicolumn{2}{@{}c@{}}{$\underbrace{\hspace*{\dimexpr10\tabcolsep+2\arrayrulewidth}\hphantom{012}}_{(1)}$} & \multicolumn{2}{@{}c@{}}{$\underbrace{\hspace*{15\tabcolsep}\hphantom{6}}_{(2)}$} & \multicolumn{2}{@{}c@{}}{$\underbrace{\hspace*{19\tabcolsep}\hphantom{6}}_{(3)}$}
                \end{tabular}
        \end{table}
%\newcolumntype{L}{>{$}l<{$}} % math-mode version of "l" column type
\end{center}

\vspace{-20pt}

(1): $N^2-\frac{N(N-1)}{2}=\frac{N(N-1)}{2}$ trasformazioni di Lorentz. (2): è sempre possibile annullare $\p g$. (3): curvatura.


\subsection{Derivata covariante della metrica}
La derivata covariante (con la connessione di Levi-Civita) ha la proprietà
\begin{equation}
        D_\mu g_{\rho\gamma}=0
\end{equation}
Possiamo verificarlo andando nel sistema localmente inerziale dove $D_\mu g_{\rho\gamma}=\p_\mu g_{\rho\gamma}=0$. Oppure direttamente:
\[\begin{split}D_\mu g_{\rho\gamma}&=\p_\mu g_{\rho\gamma}-\Gamma^\lambda_{\mu\rho}g_{\lambda\gamma}-\Gamma^\lambda_{\mu\gamma}g_{\rho\lambda}=\\&=\p_\mu g_{\rho\gamma}-\frac{1}{2}\qty\big(\p_\mu g_{\rho\gamma}+\p_\rho g_{\mu\gamma}-\p_\gamma g_{\mu\rho})-\frac{1}{2}\qty\big(\p_\mu g_{\rho\gamma}+\p_\gamma g_{\mu\rho}-\p_\rho g_{\mu\gamma})=\\&=0\end{split}\]
Quindi
\[D_\rho\qty(g_{\mu\nu}A^\mu B^\nu)=g_{\mu\nu}D_\rho A^\mu B^\nu+g_{\mu\nu}A^\mu D_\rho B^\nu\]
se $A^\mu$ e $B^\mu$ sono trasporarti parallelamente il loro prodotto scalare $g_{\mu\nu}A^\mu B^\nu$ non cambia.
