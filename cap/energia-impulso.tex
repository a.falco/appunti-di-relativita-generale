\chapter[Energia, impuso e fluidi]{Energia, impuso e fluidi}
Per una particella di massa $m$
\[\mathcal{S}=-mc\int ds=-mc^2\int dt\sqrt{1-\frac{\va{v}^2}{c^2}}\]
quindi $\S=\int dt \mathcal{L}$ dove la lagrangiana è
\[\mathcal{L}=-mc^2\sqrt{1-\frac{\va{v}^2}{c^2}}\]
l'impulso è
\[\va{p}=\frac{\var \mathcal{L}}{\var\va{v}}=\frac{1}{2}mc^2\frac{2\va{v}/c^2}{\sqrt{1-\frac{\va{v}^2}{c^2}}}=m\gamma\va{v}\]
l'energia è
\[\begin{split}E&=\va{v}\frac{\var \mathcal{L}}{\var\va{v}}-\mathcal{L}=\frac{m\va{v}^2}{\sqrt{1-\frac{v^2}{c^2}}}+mc^2\sqrt{1-\frac{v^2}{c^2}}=\\&=\gamma\left(mv^2+mc^2\left(1-\frac{v^2}{c^2}\right)\right)=m\gamma c^2\end{split}\]
Nel limite non relativistico $E\simeq mc^2+\frac{1}{2}mv^2+\ldots$.
L'energia e l'impulso formano un quadrivettore. Ricordiamo
\[u^\mu=\frac{dx^\mu}{ds}=\left(\gamma,\gamma\frac{\va{v}}{c}\right)\]
\[mcu^\mu=\left(mc\gamma,m\gamma\va{v}\right)=\left(\frac{E}{c},\va{p}\right)\]
Definiamo questo come il \textit{quadrivettore energia-impulso}
\[p^\mu\equiv mcu^\mu=\left(\frac{E}{c},\va{p}\right)\]


\section{Fluidi}
Consideriamo ora un "fluido", cioè un insieme continuo di particelle che si muovono. La densità e la velocità delle particelle dipendono da $x^\mu$. Vogliamo trovare una descrizione relativisticamente covariante ed in particolare esprimere i vincoli di conservazione dell'energia e dell'impulso.

\subsection{Fluido di polvere}
Ci sono vari tipi di fluidi, a seconda della natura delle particelle che lo compongono e delle loro interazioni. Il più semplice, che consideriamo per primo, è un \textit{fluido di polvere} (o "dust"). È composto da tante particelle massive (prendiamole tutte di massa $m$ per semplicità, ma il risultato non cambia) che si muovono ognuna su una sua linea universo. Il numero di particelle si conserva, cioè non se ne creano e nessuna scompare nel nulla. La descrizione più conveniente dei fluidi e quella \textit{euleriana} in cui non si seguono le traiettorie delle particelle (questa sarebbe \textit{lagrangiana}) ma si discutono le proprietà come funzione di $t$ e $\va{x}$, cioè localmente nello spazio tempo.

Due sono le quantità che ci servono per descrivere il fluido localmente:
\begin{enumerate}
	\item Quante particelle ci sono: per questa utilizziamo uno scalare $n(x^\mu)$ definito come la densità (numero di particelle per unità di volume) nel sistema di riferimento in cui sono ferme.
	\item Quanto velocemente si muovono: per questa invece usiamo $u^\mu(x^\nu)$ dove $u^\mu$ è la quadrivelocità (media) delle particelle che si trovano in $x^\mu$.
\end{enumerate}
Possiamo quindi costruire il quadrivettore
\[N^\mu=nu^\mu=\left(n\gamma,n\gamma\frac{\va{v}}{c}\right)\]
Questo è una quadricorrente di particelle (analogo di $J^\mu$ per la carica). La densità di particelle è $N^0=n\gamma$; nota che la densità non è uno scalare. Quindi $\int dVn\gamma$ è il numero di particelle nel volume $dV$ in un dato sistema di riferimento. La conservazione del numero si esprime come
\[\frac{d}{dt}\int dVn\gamma=-\int d\va{S}\cdot n\gamma\va{v}\]
\[\int dV\left(\frac{\partial}{\partial t}(u\gamma)+\va{\nabla}\cdot(u\gamma\va{v})\right)=0\]
quindi
\[\frac{dN^\mu}{dx^\mu}=0\]
Come avevamo visto per $J^\mu$, la quadridivergenza = 0 è l'equazione di continuità che esprime la conservazione della "quantità" portata dalla corrente in questione in forma locale.


\subsection{Tensore energia-impulso}
Ora descriviamo l'energia e l'impulso di questo fluido. La densità di energia è
\[\mathcal{E}=\frac{dE}{dV}=\underbrace{mc^2\gamma}_{\text{\parbox{4em}{energia singola}}}\underbrace{n\gamma}_{\text{densità}}\]
Vediamo che per scriverlo in forma covariante abbiamo bisogno di due quadrivettori: $\mathcal{E}=mc^2nu^0u^0$. Quindi $\mathcal{E}$ è la componente $00$ di un tensore $(2,0)$ definito da
\[T^{\mu\nu}\equiv mc^2nu^\mu u^\mu\implies\mathcal{E}=T^{00}\]
Definiamo $\rho$ come la densità di energia nel sistema di quiete del fluido. $\rho=mc^2n$ è uno scalare.
\[T^{\mu\nu}=\rho u^\mu u^\nu\]\footnote{Nota la differenza fra $\rho$ (scalare) ed $\mathcal{E}=T^{00}$.}
$T$ è un tensore $(2,0)$ simmetrico per come è stato definito. Tutte le sue componenti hanno un significato fisico.

\subsubsection{Componenti temporali}
\[\begin{split}T^{0i}=T^{i0}&=\rho u^0 u^i=mc^2n\gamma^2\frac{v^i}{c}=\\&=\underbrace{mc^2\gamma}_{\text{\parbox{4.5em}{energia singola particella}}}\underbrace{n\gamma v^i}_{\text{\parbox{4.1em}{flusso di particelle}}}\frac{1}{c}\end{split}\]
quindi questo è $1/c$ per il flusso di energia per unità di tempo attraverso una superficie ($T^{0i}=T^{i0}=\frac{W^i}{c}$). Oppure
\[T^{0i}=T^{i0}=\underbrace{m\gamma v^i}_{\text{\parbox{5em}{impulso singola particella}}}\underbrace{n\gamma}_{\text{\parbox{4.5em}{densità di particelle}}}c\]
quindi è anche $c$ per la densità di impulso ($T^{0i}=T^{i0}=c\mathcal{P}^i=\frac{W^i}{c}$).

\subsubsection{Componenti spaziali}
Per le componenti spaziali abbiamo
\[\begin{split}T^{ij}=T^{ji}&=\rho u^i u^j=mc^2n\gamma^2\frac{v^i}{c}\frac{v^j}{c}=\\&=\underbrace{m\gamma v^i}_{\text{\parbox{4em}{impulso singolo}}}\underbrace{n\gamma v^j}_{\text{flusso}}\end{split}\]
quindi è il flusso di impulso, o tensore degli sforzi.


\subsection{Leggi di conservazione}
Vediamo ora come esprimere in forma locale e covariante la conservazione di energia e impulso.

\subsubsection{Conservazione dell'energia}
La conservazione dell'energia è
\[\frac{d}{dt}\int dV\mathcal{E}=-\int d\va{S}\cdot\va{W}\]
\[\int dV\left(\frac{\partial\mathcal{E}}{\partial t}+\div\va{W}\right)=0\]
Dove $\va{W}$ è il flusso di energia. Quindi localmente deve valere
\[\frac{\partial\mathcal{E}}{\partial t}+\div\va{W}=0\]
ma abbiamo visto che $\mathcal{E}=T^{00}$, $W^i=cT^{i0}$ quindi possiamo scriverla nella forma
\[\frac{\partial T^{00}}{\partial ct}+\frac{\partial T^{i0}}{\partial x^i}=0\Longrightarrow\frac{\partial}{\partial x^\mu}T^{\mu0}=0\]

\subsubsection{Conservazione dell'impulso}
Vediamo ora la conservazione dell'impulso
\[\frac{d}{dt}\left(\int dV\mathcal{P}^i\right)=-\int dS^jT^{ji}\]
dove $\mathcal{P}^i$ è la densità di momento e $T^{ij}$ è il tensore degli sforzi. Quindi localmente
\[\frac{\p}{\p t}\mathcal{P}^i+\frac{\p}{\p x^j}T^{ji}=0\]
ma siccome $\mathcal{P}^i=\frac{1}{c}T^{0i}$ questa è equivalente a
\[\frac{\p}{\p x^\mu}T^{\mu i}=0\]

\subsubsection{Conservazione energia-impulso}
Quindi, riassumendo
\begin{equation}
	\boxed{\frac{\partial}{\partial x^\mu}T^{\mu\nu}=0}
\end{equation}
esprime localmente la conservazione dell'energia ($\partial_\mu T^{\mu0}=0$) e dell'impulso ($\partial_\mu T^{\mu i}=0$).

Queste proprietà valgono in genere per qualsiasi tipo di fluido
\[\left[T^{\mu\nu}\right]=\left(
\begin{array}{c|c c c}
\E & & c\mathcal{P}^i & \\
\hline
& & & \\
\frac{W^i}{c} & & T^{ij} & \\
& & & \end{array}\right)\]
$\p_\mu T^{\mu\nu}=0$ esprime localmente la conservazione.


\subsection{Limite non relativistico}
Consideriamo localmente il limite non relativistico
\[T^{\mu\nu}=\rho u^\mu u^\nu\qquad\rho=mc^2n\]
\[u^\mu=\left(\gamma,\gamma\frac{v^i}{c}\right)=\left(1+\order{\frac{v^2}{c^2}},\frac{v^i}{c}\left(1+\order{\frac{v^2}{c^2}}\right)\right)\]
quindi (ponendo $\gamma\approx1$)
\[T^{\mu\nu}\approx\left(
\begin{array}{c|c c c}
mc^2n & & mcv^in & \\
\hline
& & & \\
mcv^in & & mv^iv^jn & \\
& & & \end{array}\right)\]
vale quindi questa gerarchia:
\[T^{00}\gg T^{0i}\gg T^{ij}\]
questo è in genere sempre vero, anche per tipi di fluidi più generici (un'eccezione può essere se $T^{0i}$ è nullo per motivi di simmetria).


\subsection{Fluido perfetto}
Ci serve una classe di fluidi un po' più generale. Un \textit{fluido perfetto} è un fluido che nel sistema di quiete è isotropo, cioè non ha direzioni preferite. Quindi
\[[T^{\mu\nu}]_{\text{(quiete)}}=\left(
\begin{array}{c|c c c}
\rho & & 0 & \\
\hline
& & & \\
0 & & p\mathbb{I}_{3\times3} & \\
& & & \end{array}\right)\]
$\rho$ è la densità di energia e $p$ è la pressione, entrambi calcolati nel sistema di quiete. $\rho$ e $p$ sono quindi quantità scalari per definizione. Nel caso del fluido di polvere considerato prima avevamo $\rho=mc^2n$, $p=0$. In genere però dobbiamo ammettere la possibilità di una temperatura, e quindi avere energia termica e anche pressione. Utilizzando $u^\mu$ possiamo quindi scrivere il tensore $T^{\mu\nu}$ in un sistema arbitrario
\[T^{\mu\nu}=\alpha u^\mu u^\nu+\beta\eta^{\mu\nu}\]
con $\alpha$, $\beta$ da determinare. Nel sistema di quiete ($\gamma=1$) $u^\mu=(1,\va{0})$, quindi
\[\left(T^{\mu\nu}\right)_{\text{quiete}}=\left(
\begin{array}{c|c c c}
\alpha+\beta & & 0 & \\
\hline
& & & \\
0 & & -\beta\mathbb{I}_{3\times3} & \\
& & & \end{array}\right)\]
quindi $\alpha+\beta=\rho$ e $-\beta=p$. Per cui abbiamo
\begin{equation}
	\boxed{T^{\mu\nu}=(\rho+p)u^\mu u^\nu-p\eta^{\mu\nu}}
\end{equation}
La relazione fra $\rho$ e $p$ è determinata dall'equazione di stato della materia. Un fluido perfetto non relativistico ha $p/\rho=\order{\frac{v^2}{c^2}}$ dove $v$ è la velocità media delle particelle. Inoltre
\[\rho=mc^2n\left(1+\order{\frac{v^2}{c^2}}\right)\]
quindi $T^{\mu\nu}\approx mc^2nu^\mu u^\nu$ e ritroviamo al prim'ordine il tensore di un fluido di polvere. 


\section{Teoria di campo}
Discutiamo ora l'energia e l'impulso per una teoria di campo. Prendiamo un semplice esempio: un campo scalare reale massivo non interagente.

L'azione è definita dal seguente invariante di Lorentz:
\[\mathcal{S}=\int d\Omega\left(\frac{1}{2}\eta^{\mu\nu}\p_\mu\phi\,\p_\nu\phi-\frac{1}{2}m^2\phi^2\right)=\int\frac{d\Omega}{c}\mathfrak{L}\]
dove $\mathfrak{L}$ è la densità lagrangiana e $\phi$ è il campo scalare
\[\phi\left(x^\mu\right):\R^{3,1}\ra\R\]
e $m$ una costante. L'equazione di Eulero-Lagrange è
\[\frac{\p}{\p x^\mu}\qty(\frac{\var\l}{\var\p_\mu\phi})-\frac{\var\l}{\var\phi}=0\]
dalla quale otteniamo l'\textit{equazione di Klein-Gordon}:
\begin{equation}
	\left(\eta^{\mu\nu}\p_\mu\p_\nu+m^2\right)\phi=0
\end{equation}
Scriviamo separando spazio e tempo ($\p_0=\p^0$ e $\p_i=-\p^i$)
\[\S=\int c\dd{t}\dd{x}^3\left(\frac{1}{2c^2}\qty\big(\p_t\phi)^2-\frac{1}{2}\left(\grad\phi\right)^2-\frac{1}{2}m^2\phi^2\right)\]
quindi $\S=\int\dd{t}\L$ dove $\L$ è la lagrangiana
\[\L=\int\dd{x}^3\left(\frac{1}{2c}\qty\big(\p_t\phi)^2-\frac{c}{2}\left(\grad\phi\right)^2-\frac{c}{2}m^2\phi^2\right)\]
L'energia totale è
\[E=\int\dd{x}^3\left(\frac{1}{2c}\qty\big(\p_t\phi)^2+\frac{c}{2}\left(\grad\phi\right)^2+\frac{c}{2}m^2\phi^2\right)\]
L'energia si conserva, infatti:
\[\begin{split}\frac{dE}{dt}&=\int \dd{x}^3\left(\frac{1}{c}\p_t\phi\p_t^2\phi+c\grad\phi\c\grad\p_t\phi+cm^2\phi\p_t\phi\right)=\\&=\int \dd{x}^3c\,\p_t\phi\underbrace{\left(\frac{1}{c^2}\p_t^2\phi-\Delta\phi+m^2\phi\right)}_{=0\ \text{equazione del moto}}+\text{bordo}\end{split}\]
\footnote{abbiamo integrato per parti, di fatto il gradiente è passato da destra a sinistra con un segno meno e si è generato un termine di bordo}Abbiamo quindi la densità di energia
\[\mathcal{E}=\frac{1}{2c}\qty(\p_t\phi)^2+\frac{c}{2}\qty(\grad\phi)^2+\frac{c}{2}m^2\phi^2 \qquad E=\int\dd{V}\mathcal{E}\]


\subsection{Tensore energia-impulso}
Vogliamo costruire il tensore energia-impuslo corrispondente, tale che
\[E=\int\dd{V}T^{00}\qquad\text{e}\qquad\p_\mu T^{\mu\nu}=0\]
Discutiamo ora il metodo per trovare $T^{\mu\nu}$ data una generica teoria di campo, poi lo applichiamo all'esempio precedente.

\subsubsection{Generica teoria di campo}\label{subsubsec:3.2.1.1}
Prendiamo una teoria
\[\S=\frac{1}{c}\int\dd{\Omega}\mathfrak{L}\left(\phi^{(i)},\p_\mu\phi^{(i)}\right)\]
$\mathfrak{L}$ è uno scalare di Lorentz, $\phi^{(i)}$ vari campi, non necessariamente scalari. $\mathfrak{L}$ dipende dalla posizione $x^\mu$ solo attraverso $\phi$ e $\p\phi$, cioè non esplicitamente. Quindi
\[\frac{\p\l}{\p x^\mu}=\frac{\var\l}{\var\phi}\p_\mu\phi+\frac{\var\l}{\var\p_\nu\phi}\p_\nu\p_\mu\phi\]
lo riscriviamo come
\[\pdv{\l}{\xm}-\pdv{\xn}\qty(\fdv{\l}{\p_\nu\phi}\p_\mu\phi)=\fdv{\l}{\phi}\p_\mu\phi-\p_\nu\qty(\fdv{\l}{\p_\nu\phi})\p_\mu\phi\]
il termine a destra si annulla se sono soddisfatte le equazioni di Eulero-Lagrange, cioè \[\p_\nu\qty(\frac{\var\l}{\var\p_\nu\phi})-\frac{\var\l}{\p\phi}=0\] otteniamo quindi
\[\frac{\p\l}{\p x^\mu}-\frac{\p}{\p x^\nu}\left(\frac{\var\l}{\var\p_\nu\phi}\p_\mu\phi\right)=0\]
Abbiamo assunto che $\l$ non dipenda esplicitamente da $x^\mu$ (invarianza per traslazioni spaziali e temporali) e la validità nelle equazioni del moto. Riarrangiando gli indici possiamo riscrivere l'uguaglianza sopra come:
\[\frac{\p}{\p x^\mu}\left(\frac{\var\l}{\var\p_\mu\phi}\p_\rho\phi\eta^{\nu\rho}-\l\eta^{\mu\nu}\right)=0\]
cioè $\p_\mu T^{\mu\nu}=0$ definendo
\begin{equation}\label{3.1}
	\boxed{T^{\mu\nu}\equiv\frac{\var\l}{\var\p_\mu\phi}\p^\nu\phi-\eta^{\mu\nu}\l}
\end{equation}

\subsubsection{Campo scalare reale massivo non interagente}
Calcoliamo $T^{\mu\nu}$ così definito per la teoria scalare usata sopra.
\[\l=\frac{c}{2}\eta^{\mu\nu}\p_\mu\phi\p_\nu\phi-\frac{c}{2}m^2\phi^2\]
quindi
\begin{equation}
	\boxed{T^{\mu\nu}=c\p^\nu\phi\p^\mu\phi-c\eta^{\mu\nu}\left(\frac{1}{2}\eta^{\rho\gamma}\p_\rho\phi\p_\gamma-\frac{1}{2}m^2\phi^2\right)}
\end{equation}
In questo caso viene $T^{\mu\nu}=T^{\nu\mu}$ simmetrico. In genere però la definizione sopra non garantisce un $T^{\mu\nu}$ simmetrico.
\[\begin{split}T^{00}&=\frac{1}{c}\left(\p_t\phi\right)^2-c\left(\frac{1}{2c^2}(\p_t\phi)^2-\frac{1}{2}\left(\va{\nabla}\phi\right)^2-\frac{1}{2}m^2\phi^2\right)=\\&=\frac{1}{2c}(\p_t\phi)^2+\frac{c}{2}\left(\va{\nabla}\phi\right)^2+\frac{c}{2}m^2\phi^2\end{split}\]
quindi $T^{00}=\E$ dove la densità di energia $\E$ l'avevamo calcolata sopra.

\subsubsection{Simmetrizzazione}
Ad un tensore energia-impulso chiediamo che soddisfi le seguenti proprietà:
\begin{itemize}
	\item $E=\int\dd{V}T^{00}$
	\item $\mathcal{P}^i=\frac{1}{c}\int\dd{V}T^{0i}$
	\item $\p_\mu T^{\mu\nu}=0$ per avere la conservazione di $E$ e $\mathcal{P}^i$
\end{itemize}
Dato un $T^{\mu\nu}$ possiamo cambiarlo mantenendo valide le proprietà sopra:
\[\tilde{T}^{\mu\nu}=T^{\mu\nu}+\p_\rho\psi^{\rho\mu\nu}\qquad\text{con}\quad\psi^{\rho\mu\nu}=-\psi^{\mu\rho\nu}\]
la conservazione\footnote{usiamo $\p_\mu T^{\mu\nu}=0$ e l'antisimmetria di $\psi^{\rho\mu\nu}$}
\[\p_\mu\tilde{T}^{\mu\nu}=\p_\mu T^{\mu\nu}+\p_\mu\p_\rho\psi^{\rho\mu\nu}=0\]
\[\tilde{E}=\int\dd{V}\tilde{T}^{00}=\int\dd{V}\left(T^{00}+\p_\mu\psi^{\mu00}\right)=E\]
se prediamo $\psi\ra0$ all'infinito. Allo stesso modo $\tilde{\mathcal{P}}^i=\mathcal{P}^i$.

Questa libertà nella definizione di $T^{\mu\nu}$ può essere usata per portarlo nella forma "canonica" in cui $T^{\mu\nu}=T^{\nu\mu}$. In genere $T^{\mu\nu}=\frac{\var\l}{\var\p_\mu\phi}\p^\nu\phi-\eta^{\mu\nu}\l$ non è simmetrico, deve quindi essere rimaneggiato con un opportuno $\p_\mu\psi^{\mu\rho\gamma}$ per farlo diventare tale (vedi esempio elettromagnetismo).


\section{Richiami di teoria di campo classica}
Alcuni richiami di teoria dei campi classica. Nella meccanica lagrangiana con $q^{(i)}$ gradi di libertà
\[\S=\int\dd{t}\L\left(q^{(i)},\dot{q}^{(i)}\right)\qquad\var\S=0\implies\dv{t}\left(\frac{\var\L}{\var\dot{q}^{(i)}}\right)-\frac{\var\L}{\var\dot{q}^{(i)}}=0\quad\forall i\]
Nella teoria dei campi abbiamo "infiniti" gradi di libertà, ma l'idea è sempre la stessa: data un'azione le equazioni del moto sono date dall'estremizzazione $\var\S=0$.
\[\S=\int\dd{t}\dd{x}^3\mathfrak{L}\left(\phi,\p_\mu\phi\right)\]
con $\mathfrak{L}$ la densità lagrangiana. $\mathfrak{L}$ è un funzionale dei campi $\phi\left(x^\mu\right)$ e delle loro derivate.
\[\begin{split}\var\S&=\int\dd{t}\dd{x}^3\left(\frac{\var\l}{\var\phi}\var\phi+\frac{\var\l}{\var\p_\mu\phi}\var\p_\mu\phi\right)=\\&=\int\dd{t}\dd{x}^3\left(\frac{\var\l}{\var\phi}-\p_\mu\left(\frac{\var\l}{\var\p_\mu\phi}\right)\right)\var\phi+\text{bordo}\end{split}\]
Quindi le equazioni del moto sono
\[\frac{\var\l}{\var\phi}-\p_\mu\left(\frac{\var\l}{\var\p_\mu\phi}\right)=0\]
Se $\mathfrak{L}$ è uno scalare di Lorentz le equazioni saranno relativisticamente invarianti. Questo perché l'azione è uno scalare e la sua estremizzazione non dipende dalle coordinate. (Comunque la teoria dei campi si può fare anche senza invarianza relativistica.) Per interpretarla come un "continuo" di gradi di libertà
\[q^{(i)}(t)\longleftrightarrow\phi(t,\va{x})\]
quindi la $i$ corrisponde al continuo $\va{x}$. In una dimensione, per semplicità:
\[\int\dd{x}\mathfrak{L}\left(\phi,\p_x\phi,\p_t\phi\right)\simeq\sum_il\l\left(\phi^{(i)},\frac{\phi^{(i)}-\phi^{(i-1)}}{l},\dot{\phi}^{(i)}\right)=\L\]
\[\dv{t}\L\left(\frac{\var\L}{\var\dot{\phi}^{(i)}}\right)-\frac{\var\L}{\var\phi^{(i)}}=l\dv{t}\L\left(\frac{\var\L}{\var\dot{\phi}^{(i)}}\right)-l\frac{\var\L}{\var\frac{\phi^{(i)}\phi^{(i-1)}}{l}}\frac{1}{l}+l\frac{\var\L}{\var\frac{\phi^{(i+1)}\phi^{(i)}}{l}}\frac{1}{l}-l\frac{\var\L}{\var\phi^{(i)}}=0\]
Nel limite continuo diventa proprio
\[\frac{\partial}{\partial t}\left(\frac{\var\mathcal{L}}{\var\partial_t\phi}\right)+\frac{\partial}{\partial x}\left(\frac{\var\mathcal{L}}{\var\partial_x\phi}\right)-\frac{\var\mathcal{L}}{\var{\phi}}=0\]