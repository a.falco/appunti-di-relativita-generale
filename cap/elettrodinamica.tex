\chapter[Elettrodinamica classica]{Elettrodinamica classica}
Ora riprendiamo l'elettromagnetismo e lo riscriviamo in forma relativisticamente invariante. Partiamo dalle equazioni di Maxwell in unità gaussiane
\[\begin{cases}
        \vec{\nabla}\cdot\va{E}=4\pi\rho\hspace{55pt}\text{Gauss}\\
        \vec{\nabla}\times\va{E}=-\frac{1}{c}\frac{\partial\va{B}}{\partial t}\hspace{40pt}\text{Faraday, induzione}\\
        \vec{\nabla}\cdot\va{B}=0\\
        \vec{\nabla}\times\va{B}=\frac{4\pi}{c}\va{J}+\frac{1}{c}\frac{\partial\va{E}}{\partial t}\hspace{10pt}\text{Ampere + Maxwell}
    \end{cases}\]
$\rho$ e $\va{J}$ sono rispettivamente la densità di carica e la corrente. Per cariche puntiformi
\[\rho(t,\va{r})=\sum_iq_i\delta^{(3)}(\va{r}-\va{r}_i(t))\qquad \va{J}(t,\va{r})=\sum_iq_i\va{v}_i(t)\delta^{(3)}(\va{r}-\va{r}_i(t))\]
Le cariche subiscono l'effetto dei campi con la forza di Lorentz:
\[\va{F}=m\frac{d\va{v}}{dt}=q\va{E}+\frac{q}{c}\va{v}\times\va{B}\]
Le equazioni di Maxwell nel vuoto ($\rho=0$, $\va{J}=0$) hanno come soluzione le onde elettromagnetiche che si propagano alla velocità $c$. Le trasformazioni di Lorentz sono state appunto definite in modo da rispettare l'esistenza di questa velocità universale, cioè uguale in ogni sistema di riferimento. Vedremo ora che le equazioni nel vuoto sono già covarianti rispetto alle trasformazioni di Lorentz. Le trasformazioni di Lorentz e la relatività ristretta sono state scoperte proprio partendo dalla elettrodinamica. Le equazioni del moto per le particelle dovranno essere opportunamente modificate per rendere tutto relativisticamente invariante. Dobbiamo capire come i vari oggetti presenti nelle equazioni di Maxwell ($\va{E}$, $\va{B}$, $\rho$, $\va{J}$) trasformano. Prendiamo la conservazione della carica
\[\frac{\dd{Q}_{\text{tot}}}{\dd{t}}=0\implies\frac{d}{\dd{t}}\int \dd{V}\rho(t,\va{r})+\int\dd{\va{S}}\cdot\va{J}=0\]\footnote{(assumiamo il volume di integrazione indipendente dal tempo, altirmenti dovremmo utilizzare il \textit{teorema del trasporto di Reynolds})}
usando il teorema della divergenza $\int dV\left(\frac{\partial\rho}{\partial t}+\vec{\nabla}\cdot\va{J}\right)=0$ quindi localmente la conservazione della carica si esprime con l'\textit{equazione di cotinuità}:
\[\frac{\partial\rho}{\partial t}+\vec{\nabla}\cdot\va{J}=0\]
Riscrivendola
\[\frac{\partial c\rho}{\partial x^0}+\frac{\partial\va{J}}{\partial x^i}\equiv\frac{\partial J^\mu}{\partial x^\mu}=0 \qquad \text{dove}\qquad J^\mu=(c\rho,\va{J})\]
questo ci suggerisce che $c\rho$, $\va{J}$ formano un quadrivettore e l'equazione di continuità è una quadri-divergenza nulla. Per capire come trasformano i campi è conveniente considerare i potenziali $\phi$ e $\va{A}$.
\[\begin{dcases}
        \va{E}=-\grad\phi-\frac{1}{c}\frac{\partial \va{A}}{\partial t}\Longrightarrow\grad\times\va{E}=-\grad\times(\grad\phi)-\frac{1}{c}\frac{\partial\grad\times\va{A}}{dt}=-\frac{1}{c}\frac{\partial\va{B}}{\partial t}\\
        \va{B}=\grad\times\va{A}\Longrightarrow\grad\cdot\va{B}=\div(\grad\times\va{A})=0
    \end{dcases}\]
L'introduzione dei potenziali è possibile per le due equazioni di Maxwell senza sorgente. I potenziali sono fondamentali per scivere una lagrangiana che riproduca la forza di Lorentz. Partiamo dalla azione non-relativistica
\[\mathcal{S}=\int dt\left(\frac{1}{2}m\va{v}^2-q\phi+\frac{q}{c}\va{A}\cdot\va{v}\right)\]
e verifichiamo che da questa azione si trovano effettivamente le equazioni di Lorentz:
\[\frac{d}{dt}\left(\frac{\var\mathcal{L}}{\var\va{v}}\right)-\frac{\var\mathcal{L}}{\var\va{x}}=0\]
\[\frac{\var\mathcal{L}}{\var\va{v}}=m\va{v}+\frac{q}{c}\va{A}\qquad\frac{\var\mathcal{L}}{\var\va{x}}=-q\grad\phi+\frac{q}{c}\grad\left(\va{A}\cdot\va{v}\right)\]
\[\frac{d}{dt}\left(m\va{v}+\frac{q}{c}\va{A}\right)=-q\grad\phi+\frac{q}{c}\grad\left(\va{A}\cdot\va{v}\right)\]
(usiamo $\grad(\va{a}\cdot\va{b})=(\va{a}\cdot\grad)\va{b}+(\va{b}\cdot\grad)\va{a}+\va{a}\times(\grad\times\va{b})+\va{b}\times(\grad\times\va{a})$)
\[m\frac{d\va{v}}{dt}=-\frac{q}{c}\frac{d\va{A}}{dt}-q\grad\phi+\frac{q}{c}\left(\va{v}\cdot\grad\right)\va{A}+\frac{q}{c}\va{v}\times\left(\grad\times\va{A}\right)\]
(su $\va{v}$ non si deriva.) Sostituiamo $\frac{d\va{A}}{dt}=\frac{\partial\va{A}}{\partial t}+\left(\va{v}\cdot\grad\right)\va{A}$
\[m\frac{d\va{v}}{dt}=-\frac{q}{c}\frac{\partial\va{A}}{\partial t}-q\grad\phi+\frac{q}{c}\va{v}\times\left(\grad\times\va{A}\right)=q\va{E}+\frac{q}{c}\va{v}\times\va{B}\]
otteniamo Lorentz come voluto.

Questo suggerisce che la parte di interazione è già in forma relativistica, se $\phi$ e $\va{A}$ formano un covettore (usiamo $\dd{t}=\frac{\dd{x^0}}{c}$ e $\dd{t}\va{v}=\dd{\va{x}}$)
\[\begin{split}\S_\text{int}&=\int\dd{t}\left(-q\phi+\frac{q}{c}\va{A}\cdot\va{v}\right)=\int\left(-\frac{q}{c}\phi\dd{x^0}+\frac{q}{c}\va{A}\cdot\dd{\va{x}}\right)=-\frac{q}{c}\int A_\mu\dd{x^\mu}\end{split}\]
dove
\[A_\mu\equiv(\phi,-\va{A})\qquad\text{oppure}\qquad A^\mu=(\phi,\va{A})\]


\section{Forza di Lorentz relativistica}

\subsection{Equazioni del moto}
Ora per trovare la versione relativistica della forza di Lorentz dobbiamo cambiare la parte cinetica della particella:
\[\S=-mc\int ds-\frac{q}{c}\int A_\mu dx^\mu\]
facciamo la derivazione mantenendo i quadrivettori, usando $s$ come parametro della linea universo della particella
\[\var\mathcal{S}=-mc\int ds\,\frac{dx_\mu}{ds}\var\frac{dx^\mu}{ds}-\frac{q}{c}\int ds\left(A_\mu\frac{\var dx^\mu}{ds}+\var A_\mu\frac{dx^\mu}{ds}\right)\]
Usiamo $\var A_\mu=\frac{\partial A_\mu}{\partial x^\nu}\,\var x^\nu$ e inoltre integriamo per parti
\[\var\mathcal{S}=mc\int\dd{s}\dv[2]{x_\mu}{s}\var{x^\mu}+\frac{q}{c}\int\dd{s}\dv{A_\mu}{s}\var{x^\mu}-\frac{q}{c}\int\dd{s}\frac{\partial A_\mu}{\partial x^\nu}\dv{x^\mu}{s}\,\var x^\nu+\text{termini di bordo}\]
Usando $\dv{A_\mu}{s}=\frac{\partial A_\mu}{\partial x^\nu}\dv{x^\nu}{s}$ nel secondo integrale e rinominando gli indici $\mu\leftrightarrow\nu$ nel terzo integrale otteniamo
\[\var\mathcal{S}=\int ds\left(mc\dv[2]{x_\mu}{s}+\frac{q}{c}\left(\frac{\partial A_\mu}{\partial x^\nu}\dv{x^\nu}{s}-\frac{\partial A_\nu}{\partial x^\mu}\dv{x^\nu}{s}\right)\right)\var{x^\mu}+\text{bordo}\]
le equazioni del moto sono\footnote{ricordiamo che si ricavano ponendo $\var\S=0$, e quindi nel nostro caso ponendo l'integrando uguale a $0$} quindi
\begin{equation}\label{2.1}
    mc\dv[2]{x_\mu}{s}=\frac{q}{c}\left(\frac{\partial A_\nu}{\partial x^\mu}-\frac{\partial A_\mu}{\partial x^\nu}\right)\dv{x^\nu}{s}
\end{equation}


\subsection{Tensore dei campi}
\begin{equation}
    \boxed{F_{\mu\nu}\equiv\partial_\mu A_\nu-\partial_\nu A_\mu}
\end{equation}
è il tensore $(0,2)$ antisimmetrico che contiene $\vec{E}$ e $\vec{B}$. (Ricordiamo che $A_i=-A^i$)
\[F_{0i}=\partial_0A_i-\partial_iA_0=\frac{1}{c}\frac{\partial A_i}{\partial t}-\partial_i\phi=E_i\]
\[F_{ij}=\partial_iA_j-\partial_jA_i=-\epsilon_{ijk}B_k\]
\[\boxed{\left[F_{\mu\nu}\right]=\begin{pmatrix}0&E_1&E_2&E_3\\-E_1&0&-B_3&B_2\\-E_2&B_3&0&-B_1\\-E_3&-B_2&B_1&0\end{pmatrix}}\]
Riscriviamo le equazioni~\eqref{2.1} in componenti spaziotemporali\footnote{con la convenzione che le lettere greche possono valere $0,1,2,3$ e quelle latine $1,2,3$}, ricordiamo $\frac{dx^\mu}{ds}=u^\mu=\left(\gamma,\frac{\vec{v}}{c}\gamma\right)$, $\gamma=\frac{1}{\sqrt{1-\frac{v^2}{c^2}}}$
\[\begin{cases}
        mc\frac{du_0}{ds}=\frac{q}{c}F_{0i}u^i\\
        mc\frac{du_i}{ds}=\frac{q}{c}F_{i0}u^0+\frac{q}{c}F_{ij}u^j
    \end{cases}\]
\[\begin{cases}
        mc\frac{d\gamma}{ds}=\frac{q}{c^2}E_i\gamma v^i\\
        m\frac{d}{ds}(\gamma v_i)=-\frac{q}{c}E_i\gamma -\frac{q}{c}\epsilon_{ijk}B_k\frac{v^j}{c}\gamma
    \end{cases}\]
moltiplichiamo entrambe per $\frac{ds}{dt}=\frac{c}{\gamma}$
\[\begin{cases}
        mc\frac{d\gamma}{dt}=\frac{q}{c}\vec{E}\cdot\vec{v}\\
        m\frac{d}{dt}(\gamma v_i)=-qE_i-\frac{q}{c}\epsilon_{ijk}B_kv^j
    \end{cases}\]
ora usiamo $v_i=-v^i$
\begin{equation}
    \begin{dcases}
        \frac{d}{dt}\left(mc^2\gamma\right)=q\vec{E}\cdot\vec{v}\\
        \frac{d}{dt}\left(m\gamma\vec{v}\right)=q\vec{E}+\frac{q}{c}\vec{v}\times\vec{B}
    \end{dcases}
\end{equation}
la prima è la variazione dell'energia della particella $mc^2\gamma$ data dal lavoro fatto da $\vec{E}$. La seconda è la variazione del momento $m\gamma\vec{v}$ data dalla forza di Lorentz.

Le equazioni sono ora relativisticamente invarianti, l'unica cosa da modificare è la parte cinetica dell'azione della particella. Scriviamo ora l'accoppiamento in funzione di $\rho$ e $\vec{J}$. Abbiamo
\[\mathcal{S}_\text{int}=-\frac{q}{c}\int A_\mu\dd{x^\mu}\]
andando nel sistema di riferimento in cui la particella è ferma ($\dd{x^i}=0$)
\[\mathcal{S}_\text{int}=-\frac{q}{c}\int A_0\dd{x^0}\]
ma questo è anche
\[\mathcal{S}_\text{int}=-\frac{1}{c}\int\dd{x^0}\dd{x^1}\dd{x^2}\dd{x^3}A_0\rho\]
visto che $\rho=q\delta^{(3)}(\vec{r}-\vec{r}_0)$. Quindi siccome $J^\mu=(c\rho,\va{J})$, l'accoppiamento in forma invariante è (con $\dd{\Omega}=\dd{x^0}\dd{x^1}\dd{x^2}\dd{x^3}$)
\begin{equation}
    \boxed{\mathcal{S}_\text{int}=-\frac{1}{c^2}\int\dd{\Omega}A_\mu J^\mu}
\end{equation}


\section{Invarianza di gauge}
Discutiamo ora l'invarianza di gauge e come è collegata alla conservazione della carica. Dalla derivazione dei potenziali
\[\begin{dcases}
        \vec{E}=-\vec{\nabla}\phi-\frac{1}{c}\frac{\partial\vec{A}}{\partial t}\\
        \vec{B}=\vec{\nabla}\times\vec{A}
    \end{dcases}\]
sappiamo che i potenziali possono essere ridefiniti lasciando $\vec{E}$ e $\vec{B}$ invariati. $\vec{A}\rightarrow\vec{A}+\vec{\nabla}f$ non cambia $\vec{B}$ perché $\vec{\nabla}\times\left(\vec{\nabla}f\right)=0$. Per lasciare invariato anche $\vec{E}$ dobbiamo trasformare $\phi\rightarrow\phi-\frac{1}{c}\frac{\partial f}{\partial t}$. Scritto in forma covariante
\[A_\mu=(\phi,-\vec{A})\rightarrow\left(\phi-\frac{1}{c}\frac{\partial f}{\partial t},-\vec{A}-\vec{\nabla}f\right)=A_\mu-\frac{\partial f}{\partial x^\mu}\]
L'invarianza di gauge della interazione è garantita dalla conservazione della carica
\[\mathcal{S}_\text{int}\rightarrow-\frac{1}{c^2}\int d\Omega\,\left(A_\mu-\frac{\partial f}{\partial x^\mu}\right)J^\mu=\mathcal{S}_\text{int}+\frac{1}{c^2}\int d\Omega\,\frac{\partial f}{\partial x^\mu}J^\mu\]
ma
\[\frac{\partial f}{\partial x^\mu}J^\mu=\frac{\partial}{\partial x^\mu}\left(fJ^\mu\right)-f\frac{\p J^\mu}{\partial x^\mu}=\frac{\partial}{\partial x^\mu}\left(fJ^\mu\right)\]\footnote{l'ultima uguaglianza deriva dalla conservazione della carica $\partial_\mu J^\mu=0$}
quindi
\[\mathcal{S}_\text{int}\rightarrow\mathcal{S}_\text{int}+\frac{1}{c^2}\int d\Omega\,\frac{\partial}{\partial x^\mu}\left(f J^\mu\right)\]
$f J^\mu$ è un termine di bordo, non cambia le equazioni del moto.

\subsection{Equazioni di Maxwell nel vuoto}
Scrivendo i campi in funzione dei potenziali abbiamo 2 equazioni risolte automaticamente. Vediamolo in forma covariante. $F_{\mu\nu}=\partial_\mu A_\nu-\partial_\nu A_\mu$ è un tensore $(0,2)$ antisimmetrico per definizione. Possiamo costruire il tensore duale
\[\boxed{\tensor[^*]{F}{^{\mu\nu}}=\frac{1}{2}\epsilon^{\mu\nu\rho\gamma}F_{\rho\gamma}}\]
la sua quadridivergenza è nulla:
\[\partial_\mu\tensor[^*]{F}{^{\mu\nu}}=\frac{1}{2}\partial_\mu\epsilon^{\mu\nu\rho\gamma}F_{\rho\gamma}=\frac{1}{2}\epsilon^{\mu\nu\rho\gamma}\left(\partial_\mu\partial_\rho A_\gamma-\partial_\mu\partial_\gamma A_\rho\right)\]
ma siccome le derivate commutano e vengono contratte con $\epsilon^{\mu\nu\rho\gamma}$, abbiamo
\begin{equation}
    \boxed{\partial_\mu\tensor[^*]{F}{^{\mu\nu}}=0}
\end{equation}
queste equazioni sono equivalenti a quelle di Maxwell senza sorgente.
\[[\tensor[^*]{F}{^{\mu\nu}}]=\begin{pmatrix}0&-B_1&-B_2&-B_3\\B_1&0&-E_3&E_2\\B_2&E_3&0&-E_1\\B_3&-E_2&E_1&0\end{pmatrix}\]
\[\begin{dcases}\partial_\mu\tensor[^*]{F}{^{\mu0}}=0\implies\vec{\nabla}\cdot\vec{B}=0\\\partial_\mu\tensor[^*]{F}{^{\mu i}} = 0\implies\frac{1}{c}\frac{\partial\tensor[^*]{F}{^{0i}}}{\partial t} + \frac{\partial}{\partial x^j}\tensor[^*]{F}{^{ji}}=0\end{dcases}\]
quindi, dalla seconda abbiamo $-\frac{1}{c}\frac{\partial\vec{B}}{\partial t}-\vec{\nabla}\times\vec{E}=0$.


\subsection{Equazioni di Maxwell con sorgente}
Scriviamo ora le altre equazioni di Maxwell facendo la quadridivergenza di $F^{\mu\nu}$\footnote{\[\left[F^{\mu\nu}\right]=\begin{pmatrix}0&-E_1&-E_2&-E_3\\E_1&0&-B_3&B_2\\E_2&B_3&0&-B_1\\E_3&-B_2&B_1&0\end{pmatrix}\]}
\begin{equation}
    \boxed{\p_\mu F^{\mu\nu}=\frac{4\pi}{c}J^\nu}
\end{equation}
verifichiamo
\[\begin{dcases}\partial_\mu F^{\mu0}=\p_iF^{i0}=4\pi\rho\implies\vec{\nabla}\cdot\vec{E}=4\pi\rho\\\partial_0 F^{0i}+\p_jF^{ji}=\frac{4\pi}{c}J^i\implies-\frac{1}{c}\frac{\p\vec{E}}{\p t}+\vec{\nabla}\times\vec{B}=\frac{4\pi}{c}\vec{J}\end{dcases}\]
Possiamo derivare le equazioni da un'azione per $A_\mu$. L'azione deve essere invariante e i soli due quadratici nelle derivate sono
\[F_{\mu\nu}F^{\mu\nu} \qquad\qquad F_{\mu\nu}\tensor[^*]{F}{^{\mu\nu}}\]
il secondo si può dimostrare che è una derivata totale
\[F_{\mu\nu}\tensor[^*]{F}{^{\mu\nu}}=\frac{1}{2}\epsilon^{\mu\nu\rho\gamma}F_{\mu\nu}F_{\rho\gamma}=2\epsilon^{\mu\nu\rho\gamma}\partial_\mu A_\nu\partial_\rho A_\gamma=2\partial_\mu\left(\epsilon^{\mu\nu\rho\gamma}A_\nu\partial_\rho A_\gamma\right)\]
Un termine di derivata totale non modifica le equazioni classiche del moto, quindi non lo consideriamo. Prendiamo ora l'azione
\[\mathcal{S}_\text{em}+\mathcal{S}_\text{int}=\alpha\int d\Omega\,F_{\mu\nu}F^{\mu\nu}-\frac{1}{c^2}\int d\Omega\,A_\mu J^\mu\]
determiniamo $\alpha$ in modo che si riottengano le equazioni di Maxwell. Facciamo quindi la variazione rispetto $A_\mu$
\[\var(\mathcal{S}_\text{em}+\mathcal{S}_\text{int})=\int d\Omega\left(2\alpha\var F_{\mu\nu}F^{\mu\nu}-\frac{1}{c^2}\var A_\mu J^\mu\right)\]
\[\var(\mathcal{S}_\text{em}+\mathcal{S}_\text{int})=\int d\Omega\left(-4\alpha\partial_\nu\var A_\mu F^{\mu\nu}-\frac{1}{c^2}J^\mu\var A_\mu\right)\]
ora integriamo per parti
\[\var(\mathcal{S}_\text{em}+\mathcal{S}_\text{int})=\int d\Omega\left(4\alpha\partial_\nu F^{\mu\nu}-\frac{1}{c^2}J^\mu\right)\var A_\mu+\text{termini di bordo}\]
l'equazione del moto è quindi
\[\partial_\nu F^{\mu\nu}=\frac{1}{4\alpha c^2}J^\mu\]
Per ottenere $\partial_\mu F^{\mu\nu}=\frac{4\pi}{c}J^\nu$ abbiamo bisogno di imporre
\[\frac{1}{4\pi c^2}=-\frac{4\pi}{c}\implies\alpha=-\frac{1}{16\pi c}\]
Possiamo ora riassumere l'azione totale
\[\mathcal{S}_\text{tot}=\mathcal{S}_\text{em}+\mathcal{S}_\text{int}+\mathcal{S}_\text{mat}=-\frac{1}{16\pi c}\int \dd{\Omega}F_{\mu\nu}F^{\mu\nu}\underbrace{-\frac{q}{c}\int A_\mu\dd{x^\mu}-mc\int\dd{s}}_{\text{va aggiunto per ogni particella}}\]
riscrivendo un'azione come $\int d\Omega\,f(x^\mu)$ dove $f(x^\mu)$ è uno scalare, siamo sempre garantiti che le equazioni trovate dall'estremizzazione saranno covarianti.


\section{Energia e impulso}
Consideriamo ora l'energia e l'impulso del campo elettromagnetico. Discutiamolo prima separando spazio e tempo.

\subsection{Energia dei campi}
Il lavoro\footnote{si tratta in realtà della potenza} fatto sulle particelle è
\[\dv{L}{t}=W=\va{v}\cdot\left(q\va{E}+\frac{q}{c}\va{v}\times\va{B}\right)=q\va{v}\cdot\va{E}\]
oppure scritto con la corrente
\[W=\int \dd{x}^3\va{J}\cdot\va{E}\]
Interpretiamo $L$ come meno l'energia $E$ immagazzinata nel campo.
\[\begin{split}\dv{E}{t}&=-\int \dd{x}^3\va{J}\cdot\va{E}=-\int \dd{x}^3\left(\frac{c}{4\pi}\left(\grad\times\va{B}\right)\cdot\va{E}-\frac{1}{4\pi}\frac{\partial\va{E}}{\partial t}\cdot\va{E}\right)=\\&=\int \dd{x}^3\left(\frac{1}{8\pi}\frac{\p}{\p t}\left(\va{E}^2\right)+\frac{c}{4\pi}\div\left(\va{E}\times\va{B}\right)+\frac{1}{4\pi}\va{B}\frac{\p\va{B}}{\p t}\right)=\\&=\frac{d}{dt}\int_V\dd{x}^3\frac{1}{8\pi}\left(\va{E}^2+\va{B}^2\right)+\int_S\dd{\va{S}}\cdot c\frac{\va{E}\times\va{B}}{4\pi}\end{split}\]
Dove nella seconda uguaglianza abbiamo usato l'equazione di Maxwell $\grad\va{B}=\frac{4\pi}{c}\va{J}+\frac{1}{c}\frac{\p\va{E}}{\p t}$ e nella terza uguaglianza $\va{E}\cdot\left(\grad\times\va{B}\right)=\div\left(\va{B}\times\va{E}\right)+\va{B}\cdot\left(\grad\times\va{E}\right)$ e $\grad\times\va{E}=-\frac{1}{c}\frac{\p\va{B}}{\p t}$. Abbiamo quindi che la densità di energia è
\[\boxed{\mathcal{E}=\frac{1}{8\pi}\left(\va{E}^2+\va{B}^2\right)}\]
Il flusso di energia per unità di tempo (Poynting) è
\[\boxed{\va{I}=c\frac{\va{E}\times\va{B}}{4\pi}}\]


\subsection{Impulso dei campi}
Allo stesso modo troviamo l'impulso del campo. La forza esercitata sulle particelle è meno la variazione di impulso $\va{p}$ del campo:
\[\dv{\va{p}}{t}=-\left(q\va{E}+\frac{q}{c}\va{v}\times\va{B}\right)=-\int_V\dd{V}\left(\rho\va{E}+\frac{1}{c}\va{J}\times\va{B}\right)\]
\[\begin{split}\dv{\va{p}}{t}&=-\int\dd{V}\left(\frac{\div\va{E}}{4\pi}\va{E}+\frac{1}{4\pi}\left(\grad\times\va{B}\right)\times\va{B}-\frac{1}{4\pi c}\frac{\p\va{E}}{\p t}\times\va{B}\right)=\\&=\dv{t}\int\dd{V}\frac{1}{4\pi c}\va{B}\times\va{B}-\int\dd{V}\frac{1}{4\pi}\left(\div\va{E}\va{E}+\div\va{B}\va{B}+\left(\grad\times\va{B}\right)\times\va{B}+\left(\grad\times\va{E}\right)\times\va{E}\right)\end{split}\]
usiamo $\left(\grad\times\a\right)\times\a=\left(\a\c\grad\right)\c\a-\frac{1}{2}\grad\left(\a^2\right)$. Riscriviamo la parentesi nell'ultimo integrale come
\[\begin{split}&\div\va{E}\va{E}+\left(\va{E}\c\grad\right)\va{E}-\frac{1}{2}\grad\left(\va{E}\right)^2+\left(\div\va{B}\right)\va{B}+\left(\va{B}\c\grad\right)\va{B}-\frac{1}{2}\grad\left(\va{B}^2\right)=\\&=\p_iE_iE_j+E_i\p_iE_j-\frac{1}{2}\p_j\va{E}^2+\p_iB_iB_j+B_i\p_iB_j-\frac{1}{2}\p_j\va{B}^2=\\&=\p_i\left(E_iE_j-\frac{1}{2}\delta_{ij}\va{E}^2+B_iB_j-\frac{1}{2}\delta_{ij}\va{B}^2\right)\end{split}\]
ora possiamo integrare per parti
\[\left(\frac{d\va{p}}{dt}\right)_j=-\frac{d}{dt}\int_V\,dV\frac{1}{4\pi c}\left(\va{E}\times\va{B}\right)_j-\int dS^i\,\frac{1}{4\pi}\left(E_iE_j-\frac{1}{2}\delta_{ij}\va{E}^2+B_iB_j-\frac{1}{2}\delta_{ij}\va{B}^2\right)\]
quindi interpretiamo
\[\boxed{\va{\mathcal{P}}=\frac{1}{4\pi c}\va{E}\times\va{B}}\]
come densità di impulso del campo
\[\boxed{T_{ij}=\frac{1}{4\pi}\left(E_iE_j-\frac{1}{2}\delta_{ij}\va{E}^2+B_iB_j-\frac{1}{2}\delta_{ij}\va{B}^2\right)}\]
tensore degli sforzi.


\subsection{Tensore energia-impulso}
$\mathcal{E}$, $\va{I}/c$, $c\va{\mathcal{P}}$ e $T_{ij}$ devono essere scritti in maniera covariante formando il tensore energia-impulso.

\subsubsection{Derivazione dall'azione}
Deriviamo il tensore energia-impulso $T^{\mu\nu}$ partendo dall'azione.
\[\S_\text{em}=-\frac{1}{16\pi c}\int d\Omega\,F_{\mu\nu}F^{\mu\nu}\qquad\text{con}\quad F_{\mu\nu}=\p_\mu A_\nu-\p_\nu A_\mu\]
dalla teoria dei campi sappiamo che
\begin{equation}
    \boxed{T^{\mu\nu}=\p^\nu\phi^{(i)}\frac{\var\l}{\var\p_\mu\phi^{(i)}}-\eta^{\mu\nu}\l}
\end{equation}
dove la derivazione va fatta per ogni campo $\phi^{(i)}$ in $\l$. Ora $\phi^{(i)}$ sono i $4$ campi $A_\mu$. Quindi
\[T^{\mu\nu}=\p^\nu A_\rho\frac{\var\l}{\var\p_\mu A_\rho}-\eta^{\mu\nu}\l\]
\[\l=-\frac{1}{16\pi}F_{\mu\nu}F^{\mu\nu}=-\frac{1}{8\pi}\qty\big(\p_\mu A_\nu\p^\mu A^\nu-\p_\nu A_\mu\p^\mu A^\nu)\]
\[\frac{\var\l}{\var\p_\mu A_\rho}=-\frac{1}{4\pi}F^{\mu\rho}\]
\[T^{\mu\nu}=-\frac{1}{4\pi}\p^\nu A_\rho F^{\mu\rho}+\frac{1}{16\pi}\eta^{\mu\nu}F_{\rho\gamma}F^{\rho\gamma}\]
Costruiamo un tensore simmetrico aggiungendo $\frac{1}{4\pi}\partial_\rho A^\nu F^{\mu\rho}$. Questo pezzo in più si può scrivere, usando $\partial_\rho F^{\mu\rho}=0$ valida se $J^\mu=0$, come $\frac{1}{4\pi}\partial_\rho\left(A^\nu F^{\mu\rho}\right)$ quindi è della forma $\partial_\rho\psi^{\rho\mu\nu}$ con $\psi^{\rho\mu\nu}=-\psi^{\mu\rho\nu}$. Quindi il tensore nella forma simmetrica è
\begin{equation}
    \boxed{T^{\mu\nu}=-\frac{1}{4\pi}\tensor{F}{^\mu_\rho}F^{\nu\rho}+\frac{1}{16\pi}\eta^{\mu\nu}F_{\rho\gamma}F^{\rho\gamma}}
\end{equation}
Ricordiamo che
\[\left[F_{\mu\nu}\right]=\begin{pmatrix}0&E_1&E_2&E_3\\-E_1&0&-B_3&B_2\\-E_2&B_3&0&-B_1\\-E_3&-B_2&B_1&0\end{pmatrix}\qquad F_{\mu\nu}F^{\mu\nu}=-2\left(\vec{E}^2-\vec{B}^2\right)\]
verifichiamo quindi che
\[T^{00}=\frac{1}{4\pi}\vec{E}^2-\frac{1}{8\pi}\left(\vec{E}^2-\vec{B}^2\right)=\frac{1}{8\pi}\left(\vec{E}^2+\vec{B}^2\right)=\mathcal{E}\]
\[T^{0i}=T^{i0}\frac{1}{4\pi}\left(\vec{E}\times\vec{B}\right)_i=c\left(\vec{\mathcal{P}}\right)_i=\frac{1}{c}\left(\vec{I}\right)_i\]
Una proprietà del $T_{\mu\nu}$ elettromagnetico è che $[\tensor{T}{^\mu_\nu}]$ ha traccia nulla:
\[\eta_{\mu\nu}T^{\mu\nu}=-\frac{1}{4\pi}F_{\mu\nu}F^{\mu\nu}+\frac{1}{16\pi}\underbrace{\eta_{\mu\nu}\eta^{\mu\nu}}_{4}F_{\rho\gamma}F^{\rho\gamma}=0\]
quindi un flusso di onde elettromagnetiche, se isotropo, deve avere $p=\frac{1}{3}\rho$ in modo da avere $\eta_{\mu\nu}(\rho+p)u^\mu u^\nu-\rho\eta_{\mu\nu}\eta^{\mu\nu}=\rho+p-4\rho=0$.

\subsubsection{Conservazione di $T^{\mu\nu}_\text{tot}$}
Se consideriamo tutta l'azione $\mathcal{S}_\text{em}+\mathcal{S}_\text{int}+\mathcal{S}_\text{mat}$, allora $T^{\mu\nu}_\text{em}$ e $T^{\mu\nu}_\text{mat}$ non si conservano singolarmente, ma comunque deve valere
\begin{equation}
    \p_\mu\qty\big(T^{\mu\nu}_\text{em}+T^{\mu\nu}_\text{mat})=0
\end{equation}
dove $T^{\mu\nu}_\text{mat}=mc^2nu^\mu u^\nu$ per fluido di particelle di massa $m$ non interagenti. Prendiamo ogni particella con carica $q$, $N^\mu=nu^\mu$, $J^\mu=cqN^\mu$ è la quadricorrente. Possiamo anche scrivere $T^{\mu\nu}_\text{mat}$ per una singola particella, per questo usiamo $n=\gamma^{-1}\delta^{(3)}\left(x^i-x^i(s)\right)$ dove $x^\mu(s)$ è la traiettoria della particella. In questo caso $T^{\mu\nu}_\text{mat}$ ha supporto solo sulla linea universo della particella. Possiamo anche scrivere $T^{\mu\nu}_\text{mat}=mc^2N^\mu u^\nu$. Ora calcoliamo la divergenza
\[\p_\mu T^{\mu\nu}_\text{mat}=mc^2\p_\mu N^\mu u^\nu+mc^2N^\mu\p_\mu u^\nu\]
ma $\p_\mu N^\mu=0$ per conservazione delle particelle. Quindi
\[\p_\mu T^{\mu\nu}_\text{mat}=mc^2\gamma^{-1}\delta^{(3)}\left(x^i-x^i(s)\right)\frac{dx^\mu}{ds}\frac{du^\nu}{dx^\mu}\]
usiamo l'equazione del moto \ref{2.1} $mc^2\frac{du^\nu}{ds}=qF^\nu_{\ \rho}u^\rho$
\[\p_\mu T^{\mu\nu}_\text{mat}=q\gamma^{-1}\delta^{(3)}\left(x^i-x^i(s)\right)F^\nu_{\ \rho}u^\rho=\frac{1}{c}F^\nu_{\ \rho}J^\rho\]
dove $J^\rho$ è la corrente di carica. Ora calcoliamo la divergenza di $T^{\mu\nu}_\text{em}$
\[\begin{split}\p_\mu T^{\mu\nu}_\text{em}&=\p_\mu\left(-\frac{1}{4\pi}F^\mu_{\ \rho}F^{\nu\rho}+\frac{1}{16\pi}\eta^{\mu\nu}F^{\rho\gamma}F_{\rho\gamma}\right)=\\&=\frac{1}{4\pi}\left(-\p_\mu F^\mu_{\ \rho}F^{\nu\rho}-F^\mu_{\ \rho}\p_\mu F^{\nu\rho}+\frac{1}{2}\p^\nu F^{\rho\gamma}F_{\rho\gamma}\right)\end{split}\]
prendiamo gli ultimi due termini
\[\begin{split}-F_{\mu\rho}\p^\mu F^{\nu\rho}+\frac{1}{2}F_{\rho\mu}\p^\nu F^{\rho\mu}&=-F_{\mu\rho}\p^\mu F^{\nu\rho}-\frac{1}{2}F_{\rho\mu}\left(\p^\rho F^{\mu\nu}+\p^\mu F^{\nu\rho}\right)=\\&=-\frac{1}{2}F_{\rho\mu}\p^\mu F^{\nu\rho}-\frac{1}{2}F_{\rho\mu}\p^\rho F^{\mu\nu}=0\end{split}\]
dove nella prima uguaglianza abbiamo usato $\p_\mu F_{\nu\mu}+\p_\rho F_{\nu\mu}+\p_\nu F_{\mu\rho}=0$ equivalente a $\p_\mu\tensor[^*]{F}{^{\mu\nu}}=0$. Quindi rimane
\[\p_\mu T^{\mu\nu}_\text{em}=-\frac{1}{4\pi}\p_\mu \tensor{F}{^\mu_\rho}F^{\nu\rho}=-\frac{1}{c}J_\rho F^{\nu\rho}=-\frac{1}{c}\tensor{F}{^\nu_\rho}J^\rho\]
abbiamo quindi dimostrato
\[\p_\mu\left(T^{\mu\nu}_\text{em}+T^{\mu\nu}_\text{mat}\right)=0\]


\section{Esercizi}
\subsection{Trasformazioni di Lorentz per E e B} %per $\va{E}$ e $\va{B}$
Scriviamo le trasformazioni di Lorentz per i campi $\va{E}$, $\va{B}$, per un sistema $O'$ che si muove con velocità $-v\va{x}$ rispetto a $O$.
\[\left[F^{\mu\nu}\right]=\begin{pmatrix}0&-E_1&-E_2&-E_3\\E_1&0&-B_3&B_2\\E_2&B_3&0&-B_1\\E_3&-B_2&B_1&0\end{pmatrix}\qquad[\Lambda^\mu_{\ \nu}]=\begin{pmatrix}\cosh\alpha&\sinh\alpha&0&0\\\sinh\alpha&\cosh\alpha&0&0\\0&0&1&0\\0&0&0&1\end{pmatrix}\]
\[F'^{\mu\nu}=\Lambda^\mu_{\ \rho}\Lambda^\nu_{\ \gamma}F^{\rho\gamma}\]
\[\begin{gathered}
        [F'^{\mu\nu}]=[\Lambda^\mu_{\ \rho}]\cdot[F^{\rho\gamma}]\cdot[\Lambda^\nu_{\ \gamma}]^t=\\=\begin{pmatrix}\cosh\alpha&\sinh\alpha&0&0\\\sinh\alpha&\cosh\alpha&0&0\\0&0&1&0\\0&0&0&1\end{pmatrix}\begin{pmatrix}-E_1\sinh\alpha&-E_1\cosh\alpha&-E_2&-E_3\\E_1\cosh\alpha&E_1\sinh\alpha&-B_3&B_2\\E_2\cosh\alpha+B_3\sinh\alpha&E_2\sinh\alpha+B_3\cosh\alpha&0&-B_1\\E_3\cosh\alpha-B_2\sinh\alpha&E_3\sinh\alpha-B_2\cosh\alpha&B_1&0\end{pmatrix}=\\=\begin{pmatrix}0&-E_1&-E_2\cosh\alpha-B_3\sinh\alpha&-E_3\cosh\alpha+B_2\sinh\alpha\\0&0&-E_2\sinh\alpha-B_3\cosh\alpha&-E_3\sinh\alpha+B_2\cosh\alpha\\0&0&0-B_1\\0&0&0&0\end{pmatrix}
    \end{gathered}\]
Quindi
\[\begin{cases}E'_1=E_1\\E'_2=E_2\cosh\alpha+B_3\sinh\alpha\\E'_3=E_3\cosh\alpha-B_2\sinh\alpha\end{cases}\qquad\begin{cases}B'_1=B_1\\B'_2=B_2\cosh\alpha-E_3\sinh\alpha\\B'_3=B_3\cosh\alpha+E_2\sinh\alpha\end{cases}\]
Possiamo verificare direttamente che $\va{E}^2-\va{B}^2$ e $\va{E}\cdot\va{B}$ sono invarianti.

Vediamo un esempio. In un sistema di riferimento $O$ abbiamo un filo percorso da una corrente $I$ lungo l'asse $\hat{x}$.
\[2\pi RB=\frac{4\pi}{c}I\quad\text{Ampère}\]
\[\va{B}=\frac{2}{c}I\frac{-z\hat{y}+y\hat{z}}{y^2+z^2}\]
Una particella carica sente una forza
\[F=\frac{q}{c}\va{v}\times\va{B}=\frac{d\left(m\gamma\va{v}\right)}{dt}\]
Prendiamo $\va{v}=v\hat{x}$, e la particella in $z=0$. $\va{F}=-\hat{y}\left(q\frac{v}{c}\frac{2I}{cy}\right)$. Ora andiamo nel sistema di riferimento $O'$ solidale con la particella. $\va{F}'=\frac{d\va{p}'}{dt'}=\gamma\va{F}$ ma siccome $\va{v}'=0$ la forza non può altro che essere dovuta al campo elettrico $\va{F}'=q\va{E}'$. Quindi deve valere $qE'_y=-\gamma q\frac{v}{c}B_z$ questa è proprio la trasformazione dei campi $E'_y=e_y\cosh\alpha+B_z\sinh\alpha$ dove $E_y=0$ e $\sinh\alpha=-\gamma\frac{v}{c}$. Nel sistema $O$ $\va{E}=0$, quindi la carica totale sul filo è nulla. Per avere una corrente $I$ e carica nulla servono due fluidi: uno carico con densità lineare $\lambda$ e velocità $w$, tale che $I=\lambda w$, e uno carico e fermo con densità lineare $-\lambda$. Nel sistema $O'$ i due fluidi hanno densità diverse e questo genera il campo elettrico. Il secondo fluido ha, nel sistema $O'$ una velocità $-v$ e una densità $-\gamma_v\lambda$, dove $\gamma=\frac{1}{\sqrt{1-\frac{v^2}{c^2}}}$ è dovuto alla concatenzazione delle lunghezze. Il primo fluido ha una velocità $w'=\frac{w-v}{1-\frac{wv}{c^2}}$ e una densità $\frac{\gamma_{w'}}{\gamma_w}\lambda$. Quindi la densità di caroca totale sul filo è
\[\lambda\left(\frac{\gamma_{w'}}{\gamma_w}-\gamma_v\right)=\lambda\gamma_v\left(1-\frac{vw}{c^2}-1\right)=-\lambda\gamma_v\frac{vw}{c^2}=-I\gamma\frac{v}{c^2}\]
Quindi in $z=0$ $E'_y=2\frac{\lambda'}{y}=-2y\gamma_v\frac{v}{c^2}\frac{1}{y}$ ma questo è proprio uguale a $-\gamma\frac{v}{c}B_z=-\gamma\frac{zv}{c^2}I\frac{1}{y}$. Nel sistema $O'$ abbiamo anche delle correnti e quindi un campo $\va{B}'$.
\[I'=\gamma_v\lambda v+\frac{\gamma_{w'}}{\gamma_w}\lambda w'=\gamma_v\lambda\left(v+\left(1-\frac{wv}{c^2}\right)w'\right)=\gamma_v\lambda w=\gamma_vI\]
E questo è proprio consistente con la trasformazione $B'_\perp=\gamma B_\perp$. Se $\va{E}\cdot\va{B}=0$ è sempre possibile scegliere un sistema di riferimento in cui uno fra $\va{E}$ e $\va{B}$ è nullo. Se $\va{E}^2-\va{B}^2>0$ possiamo annullare $\va{B}$ e viceversa se $\va{E}^2-\va{B}^2<0$ possiamo annullare $\va{E}$. Prendiamo ad esempio un sistema $O$ in cui $\va{E}=E_2\hat{x}_2$, $\va{B}=B_3\hat{x}_3$ con $|E_2|>|B_3|$. Usiamo la trasformazione sopra con $\tanh\alpha=-\frac{B_3}{B_2}$
\[\begin{cases}E'_1=0\\E'_2=E_2\cosh\alpha+B_3\sinh\alpha\\E'_3=0\end{cases}\qquad\begin{cases}B'_1=0\\B'_2=0\\B'_3=B_3\cosh\alpha+E_2\sinh\alpha=0\end{cases}\]
\[E'_2=E_2\sqrt{1-\frac{B_3^2}{E_2^2}}\] %provare un'unica equazione ma su due linee
