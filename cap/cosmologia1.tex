\chapter[Cosmologia 1]{Cosmologia (parte 1)}
Il \textit{principio cosmologico} asserisce che l'universo, a distanze sufficientemente grandi, è omogeneo e isotropo. Dobbiamo immaginare l'universo come un gas, in cui le particelle sono le galassie. Come in un gas, a distanze grandi rispetto alla distanza fra due componenti fondamentali, osserviamo una distribuzione del tutto omogenea. Per ora possiamo considerare il principio cosmologico come un'assunzione, che di certo riduce di gran lunga i gradi di libertà da calcolare e di fatto rende il modello risolubile analiticamente (o quasi). Vedremo poi quali osservabili fisiche confermano la validità di questa assunzione. Quando parliamo di omogeneità e isotropia ci riferiamo alla parte spaziale della metrica.


\section{Spazi massimamente simmetrici}
Studiamo ora i vari esempi di metriche spaziali tridimensionali caratterizzate dalle proprietà di omogeneità e isotropia.

\subsection{Spazio euclideo $R^3$}
La metrica euclidea è l'esempio più semplice: $\dd{l}^2=\dd{x^1}^2+\dd{x^2}^2+\dd{x^3}^2$. Questo spazio è \textit{omogeneo} nel senso che dati due punti distinti qualsiasi esiste sempre una trasformazione $x^i\ra x'^i(x^j)$ che porta un punto in un altro e lascia la metrica invariata $g'_{ij}(x^k)=g_{ij}(x^k)$. In questo caso la trasformazione cercata non è altro che una semplice traslazione rigida dello spazio. I tre vettori di Killing che generano le traslazioni sono i campi vettoriali costanti $\xi^i_{(1)}=(1,0,0)$, $\xi^i_{(2)}=(0,1,0)$, $\xi^i_{(3)}=(0,0,1)$ e banalmente soddisfano $D_i\xi_j+D_j\xi_i=0$.

Lo spazio euclideo è anche \textit{isotropo}, nel senso che dato un punto qualsiasi, e due direzioni (cioè due vettori unitari $\va{n}_1$ e $\va{n}_2$ definiti nel punto dato) possiamo sempre trovare una simmetria della metrica che lascia il punto invariato e trasforma le direzioni una nell'altra. Nel caso euclideo queste trasformazioni sono le rotazioni attorno al punto dato. Nel caso in cui il punto fisso sia $x^i=(0,0,0)$ i vettori di Killing che generano le rotazioni sono $\xi^i_{(12)}=(-x^2,x^1,0)$, $\xi^i_{(23)}=(0,-x^3,x^2)$, $\xi^i_{(31)}=(x^3,0,-x^1)$. Possiamo verificare $D_i\xi_{(12)j}+D_j\xi_{(12)i}=0$; l'unica componente non banale è $D_2\xi_{(12)1}+D_1\xi_{(12)2}=\p_2\xi_{(12)1}+\p_2\xi_{(12)2}=-1+1=0$. Abbiamo quindi $6$ vettori di Killing linearmente indipendenti. Tre generano le traslazioni $\mathbb{R}^3$ e tre generano le rotazioni $SO(3)$.

$\frac{d(d+1)}{2}(=6\text{ per }d=3\text{ dimensioni})$ è il massimo numero di vettori di Killing indipendenti che si possono avere. In questo caso lo spazio si dice \textit{massimamente simmetrico}. In generale abbiamo la seguente definizione:
\begin{definition}[Spazio massimamente simmetrico]
	Uno spazio è detto massimamente simmetrico se contiene il massimo numero di vettori di Killing, corrispondente a quello di $\mathbb{R}^n$ con la metrica euclidea usuale.
\end{definition}

Lo \textit{spazio piatto} non è l'unico omogeneo e isotropo. Per ottenere gli altri è conveniente considerare un sottospazio tridimensionale di uno spazio più grande piatto quadridimensionale.


\subsection{Spazio sferico $S^3$}
Prendiamo $\dd{L}^2=\dd{x^1}^2+\dd{x^2}^2+\dd{x^3}^2+\dd{x^4}^2$ e una tre-sfera $S^3$ definita da ${x^1}^2+{x^2}^2+{x^3}^2+{x^4}^2=L^2$ dove $L$ e il raggio della sfera. Se parametrizziamo lo spazio con $x^1$, $x^2$, $x^3$ e $x^4=\sqrt{L^2-{x^1}^2-{x^2}^2-{x^3}^2}$ abbiamo che la metrica indotta è
\[\dd{l}^2=\dd{x^1}^2+\dd{x^2}^2+\dd{x^3}^2+\frac{1}{L^2-r^2}\qty\Big(x^1\dd{x^1}+x^2\dd{x^2}+x^3\dd{x^3})^2\]
dove $r=\sqrt{{x^1}^2+{x^2}^2+{x^3}^2}$. Nel limite $L\ra\infty$ riotteniamo la metrica euclidea. Questo spazio eredita tutte le simmetrie dello spazio in cui è immerso che lasciano invariato $L^2={x^1}^2+{x^2}^2+{x^3}^2+{x^4}^2$. Abbiamo quindi il gruppo delle rotazioni $SO(4)$, nota che questo spazio ha dimensione $6$.

Lo spazio $S^3$ è uno spazio omogeneo e isotropo. Con le rotazioni dello spazio quadridimensionale $SO(4)$ possiamo sempre portare un punto di $S^3$ in un altro o, lasciando fisso un punto, trasformare le direzioni una nell'altra. $S^3$ è quindi anch'esso uno spazio massimamente simmetrico, ma diverso dallo spazio euclideo, le simmetrie, seppur $6$, generano un gruppo diverso, inoltre $S^3$ ha una curvatura non nulla.

La curvatura per gli spazi massimamente simmetrici può essere espressa solo in funzione dello scalare di curvatura $R$
\[R_{ijkt}=\frac{R}{d(d-1)}\qty\Big(g_{ik}g_{jt}-g_{it}g_{jk}) \qquad R_{ij}=\frac{R}{d}g_{ij}\]
con $R$ costante. Nell'intorno di $(x^1,x^2,x^3)=(0,0,0)$ abbiamo ($r=0$)
\[\dd{l}^2=\delta_{ij}\dd{x^i}\dd{x^j}+\frac{1}{L^2}x^ix^j\dd{x^i}\dd{x^j}\]
Il punto $(0,0,0)$ ha\footnote{\[\qty[g^{ij}]=\frac{1}{L^2}\mqty(L^2+{x^1}^2 & x^1x^2 & x^1x^3 \\ x^2x^1 & L^2+{x^2}^2 & x^2x^3 \\  x^3x^1 & x^3x^2 & L^2+{x^3}^2) \qquad \qty[g^{ij}]=\delta_{ij}+\frac{x^ix^j}{L^2}\]} $\p g=0$ quindi $\Gamma=0$. Il tensore di Riemann è quindi
\[R_{ijkt}=\frac{1}{2}\qty\Big(\p_j\p_kg_{it}+\p_i\p_tg_{jk}-\p_i\p_kg_{jt}-\p_j\p_tg_{ik})=\frac{1}{L^2}\qty\Big(\delta_{ik}\delta_{jt}-\delta_{it}\delta_{jk})\]
abbiamo quindi\footnote{$R_{ijkt}=\frac{1}{L^2}\qty\Big(\delta_{ik}\delta_{jt}-\delta_{it}\delta_{jk})$, $R_{jt}=g^{ik}R_{ijkt}=\frac{1}{L^2}\qty\Big(\tensor{\delta}{^k_k}\delta_{jt}-\tensor{\delta}{^k_t}\delta_{jk})$, $R=g^{jt}R_{jt}=\frac{1}{L^2}\qty\Big(\tensor{\delta}{^k_k}\tensor{\delta}{^t_t}-\tensor{\delta}{^k_t}\tensor{\delta}{^t_k})=\frac{1}{L^2}\qty\Big(\tensor{\delta}{^k_k}\tensor{\delta}{^t_t}-\tensor{\delta}{^k_k})=\frac{1}{L^2}\qty\Big(\tensor{\delta}{^k_k}\qty(\tensor{\delta}{^t_t}-1))=\frac{6}{L^2}$, dove abbiamo fatto la traccia di $\delta$, $\tensor{\delta}{^k_k}=3$.}
\[R=\dfrac{6}{L^2}>0\]

Se usiamo coordinate radiali $r=\sqrt{{x^1}^2+{x^2}^2+{x^3}^2}$, $\theta=\arctan\frac{\sqrt{{x^1}^2+{x^2}^2}}{x^3}$, $\vp=\arctan\frac{x^2}{x^1}$, $x^4=\sqrt{L^2-r^2}$, $\dd{x^4}=\frac{-r\dd{r}}{\sqrt{L^2-r^2}}$ la metrica è
\[\dd{l}^2=\dd{r}^2+r^2\qty\Big(\dd{\theta}^2+\sin^2\theta\dd{\vp}^2)+\frac{r^2\dd{r}^2}{L^2-r^2}=\qty(\frac{1}{1-\frac{r^2}{L^2}})\dd{r}^2+r^2\qty\Big(\dd{\theta}^2+\sin^2\theta\dd{\vp}^2)\]
Più avanti useremo anche una forma riscalata di $r\ra\tilde{r}=r/L$
\[\dd{l}^2=L^2\qty(\frac{\dd{\tilde{r}}^2}{1-\tilde{r}^2}+\tilde{r}^2\qty(\dd{\theta}^2+\sin^2\theta\dd{\vp}^2))\]
Un'altra coordinata utile è l'angolo $\chi$ definito da $\chi=\arctan\frac{r}{x^4}$, $\chi=\arcsin\frac{r}{L}$, $\dd{\chi}=\frac{1}{1-\frac{r^2}{L^2}}\frac{\dd{r}}{L}$, quindi
\[\dd{l}^2=L^2\qty\Big(\dd{\chi}^2+\sin^2\chi\qty(\dd{\theta}^2+\sin^2\theta\dd{\vp}^2)) \qquad \mqty{0<\chi<\pi\\0<\theta<\pi\\0\leq\vp\leq2\pi}\]
nota che la coordinata $r$ da $0$ a $L$ ricopre solo una metà\footnote{perché sia $r$ che $x^4$ sono positivi, il codominio dell'arcotangente è quindi $(0,\pi/2)$}, $\chi$ da $0$ a $\pi/2$. Lo spazio $S^3$ ha volume totale finito
\[V_{S^3}=L^3\int_0^\pi\dd{\chi}\int_0^\pi\dd{\theta}\int_0^{2\pi}\dd{\vp}\sin^2\chi\sin\theta=L^3\frac{\pi}{2}2\,2\pi=2\pi^2L^3\]
Lo spazio è finito, ma senza bordo; si dice anche \textit{spazio chiuso}.


\subsection{Spazio iperbolico $H^3$}
L'ultimo caso di spazio massimamente simmetrico è quello con curvatura costante negativa. Anche in questo caso possiamo ottenerlo come sottospazio di uno piatto quadridimensionale. Ora lo spazio più grande è Minkowski $\dd{s}^2=-\dd{x^0}^2+\dd{x^1}^2+\dd{x^2}^2+\dd{x^3}^2$ e il sottospazio è un iperboloide di tipo spazio
\[x^0=\sqrt{L^2+{x^1}^2+{x^2}^2+{x^3}^2} \qquad \dd{x^0}=\frac{1}{\sqrt{L^2+r^2}}\qty\Big(x^1\dd{x^1}+x^2\dd{x^2}+x^3\dd{x^3})\]
La metrica indotta è \[\dd{l}^2=\dd{x^1}^2+\dd{x^2}^2+\dd{x^3}^2-\frac{1}{L^2+r^2}\qty\Big(x^1\dd{x^1}+x^2\dd{x^2}+x^3\dd{x^3})^2\]
Ora $r$ non è limitato, $0\leq r<\infty$. Attorno a $(0,0,0)$ $\dd{l}^2=\delta_{ij}\dd{x^i}\dd{x^j}-\frac{1}{L^2}\qty(x^i\dd{x^i})^2$ e la curvatura è
\[R_{ijkt}=-\frac{1}{L^2}\qty\Big(\delta_{ik}\delta_{jt}-\delta_{jk}\delta_{it}) \qquad R=-\frac{6}{L^2}\]
Quindi questo è uno spazio massimamente simmetrico con curvatura negativa. Le simmetrie di $\mathbb{R}^{3,1}$ che lasciano invariato il sottospazio sono le trasformazioni di Lorentz $SO(3,1)$ e sono proprio $6$.

In coordinate radiali $x^0=\sqrt{L^2+r^2}$, $\dd{x^0}=\frac{r\dd{r}}{\sqrt{L^2+r^2}}$, quindi
\[\dd{l}^2=\dd{r}^2+r^2\qty(\dd{\theta}^2+\sin^2\!\theta\dd{\vp}^2)-\frac{r^2\dd{r}^2}{L^2+r^2}=\frac{1}{1+\frac{r^2}{L^2}}\dd{r}^2+r^2\qty(\dd{\theta}^2+\sin^2\!\theta\dd{\vp}^2)\]
con la coordinata $\tilde{r}=r/L$
\[\dd{l}^2=L^2\qty(\frac{\dd{\tilde{r}^2}}{1+\tilde{r}^2}+\tilde{r}^2\qty(\dd{\theta}^2+\sin^2\!\theta\dd{\vp}^2))\]
Lo spazio ottenuto si chiama \textit{spazio iperbolico} $H^3$. Con la coordinata angolare $\chi=\text{arcsinh}\frac{r}{L}$, $\dd{\chi}=\frac{1}{1+\frac{r^2}{L^2}}\frac{\dd{r}}{L}$
\[\dd{l}^2=L^2\qty\Big(\dd{\chi}^2+\sinh^2\!\chi\qty(\dd{\theta}^2+\sin^2\!\theta\dd{\vp}^2)) \qquad 0\leq\chi<\infty\]
Al contrario di $S^3$, $H^3$ è uno spazio con volume infinito, per questo è chiamato anche \textit{spazio aperto}.

In generale possiamo scrivere
\begin{equation}
	\boxed{\dd{l}^2=L^2\qty(\frac{\dd{\tilde{r}^2}}{1-k\tilde{r}^2}+\tilde{r}^2\qty(\dd{\theta}^2+\sin^2\!\theta\dd{\vp}^2))}
\end{equation}
con $k=-1$ per $H^3$, $0$ per $R^3$, $1$ per $S^3$.


\section{Legge di Hubble}
\subsection{Metrica di Friedmann-Robertson-Walker}
Ora passiamo allo spazio-tempo quadridimensionale. Se imponiamo che ci siano sezioni spaziali omogenee e isotrope
\[\dd{s}^2=g_{00}\dd{x^0}^2+2g_{0i}\dd{x^0}\dd{x^i}+L^2\qty(\frac{\dd{r}^2}{1-kr^2}+r^2\dd{\Omega}^2)\]
con $\dd{\Omega}^2=\dd{\theta}^2+\sin^2\theta\dd{\vp}^2$. $g_{0i}$ deve essere nullo perché altrimenti non avremmo isotropia\footnote{In caso contrario, se $g_{0i}\neq0$ avremmo una derivata non nulla lungo la direzione $i$.}. $g_{00}$ e $L$ devono dipendere solo da $x^0$ per l'omogeneità. Possiamo quindi definire la coordinata temporale in modo che $g_{00}(x^0)\dd{x^0}^2=c^2\dd{t}^2$. Otteniamo quindi la metrica
\begin{equation}
	\boxed{\dd{s}^2=c^2\dd{t}^2-a^2(t)\qty(\frac{\dd{r}^2}{1-kr^2}+r^2\dd{\Omega}^2) \qquad k=-1,0,1}
\end{equation}
questa si chiama \textit{metrica di Friedmann-Robertson-Walker}. L'omogeneità e l'isotropia spaziale hanno ridotto il problema alla scelta di $k$ fra $-1$, $0$, $1$, e alla determinazione della funzione $a(t)$.


\subsection{Derivazione della legge di Hubble}
Un altro modo per scegliere la coordinata temporale è $c\dd{t}=a(t)\dd{\eta}$, dove $\eta$ è il \textit{tempo conforme}\footnote{Con il cambio di variabili $\eta=\int\frac{\dd{t}}{\tilde{a}(t)}c\equiv f(t)$ definiamo $\tilde{a}(t)=\tilde{a}\qty(f^{-1}(\eta))\equiv a(\eta)$. Con abuso di notazione indicheremo con la stessa lettera $a$ sia la funzione $\tilde{a}(t)$ che la funzione $a(\eta)$.}
\begin{equation}\label{13.1}
	\dd{s}^2=a^2(\eta)\qty(\dd{\eta}^2-\frac{\dd{r}^2}{1-kr^2}-r^2\dd{\Omega}^2)
\end{equation}
In questo modo abbiamo fattorizzato la dipendenza temporale, dentro le parentesi abbiamo una metrica statica. Se vogliamo trovare le traiettorie dei raggi luminosi $\dd{s}^2=0$ il fattore $a(\eta)$ è irrilevante. Quindi, come per le metriche statiche, due raggi che partono con un certo $\var\eta$ arrivano con lo stesso $\var\eta$. Un osservatore fermo con $r$, $\theta$, $\vp$ fisso misura un tempo proprio $\dd{\tau}=a(\eta)\dd{\eta}$ e la frequenza di un segnale vista da un osservatore fermo è $\nu=1/\var{\tau}$ dove $\var{\tau}$ è la separazione fra due massimi dell'onda misurata con il tempo proprio. Per cui il rapporto fra la frequenza osservata e quella emessa è
\[\frac{\nu_o}{\nu_e}=\frac{\var{\tau}_e}{\var{\tau}_o}=\frac{a(\eta_e)\var{x^0}}{a(\eta_o)\var{x^0}}=\frac{a(\eta_e)}{a(\eta_o)}\]
Per un universo in espansione $a(\eta_o)>a(\eta_e)$ e quindi i segnali vengono redshiftati in maniera proporzionale a $a(\eta)$. Questo aspetto fisico è molto utile perché è direttamente collegato ad un'osservabile fisica, cioè la relazione fra distanza e frequenza delle sorgenti luminose. Mettiamo l'osservatore nel centro delle coordinate spaziali
\[\dd{s}^2=a^2(\eta)\qty\Big(\dd{\eta}^2-\dd{\chi}^2-f(\chi)\dd{\Omega}^2) \qquad f(\chi)=\begin{cases}
\sin^2\chi & k=+1 \quad 0\leq\chi<2\pi\\
\chi^2 & k=0\phantom{+} \quad 0\leq\chi<\infty\\
\sinh^2\chi & k=-1 \quad 0\leq\chi<\infty
\end{cases}\]
il cono luce che arriva nel punto $(\eta,\chi)=(\eta_0,0)$ è dato da $(\eta,\chi=\eta_0-\eta, \theta=\text{cost}, \vp=\text{cost})$. Questa è la sezione dell'universo passato che possiamo osservare attraverso segnali luminosi.
\[\frac{\nu_o}{\nu_e}=\frac{a(\eta_0-(\eta_0-\eta))}{a(\eta_0)}\simeq1-\qty(\eta_0-\eta)\frac{1}{a(\eta_0)}\dv{a}{\eta} (\eta_0)\]
dove nell'ultimo passaggio abbiamo considerato $\eta_0-\eta$ piccolo. In questa approssimazione la distanza spaziale fra il punto di emissione e quello di assorbimento è $l\simeq a(\eta_0)\chi=a(\eta_0)(\eta_0-\eta)$. Abbiamo quindi
\[\frac{\nu_o}{\nu_e}=1-\frac{\var{\nu}}{\nu_e}\simeq1-\frac{l}{a^2}\dv{a}{\eta}\]
chiamiamo
\[\qquad \frac{\var{\nu}}{\nu_o}=\frac{\nu_e-\nu_o}{\nu_o}\equiv z \qquad z\simeq\frac{l}{a^2}\dv{a}{\eta}=\frac{l}{c}\frac{\dot{a}}{a}\]
dove abbiamo usato $a\dd{\eta}=c\dd{t}$. Chiamando $H\equiv\frac{\dot{a}}{a}$ otteniamo la \textit{legge di Hubble}
\begin{equation}
	\boxed{z=\frac{Hl}{c}}
\end{equation}

\subsection{Misura della costante di Hubble}
Se abbiamo delle sorgenti luminose "standard", di cui cioè conosciamo la frequenza di emissione e la luminosità assoluta, possiamo calcolare sia $z$ che $l$ e quindi avere una misura sperimentale del parametro
\begin{equation}\label{13.5}
	%\boxed{
		H_0\equiv\frac{\dot{a}(t_0)}{a(t_0)}=\frac{zc}{l}%}
\end{equation}
$H$ è la \textit{costante di Hubble}, "$0$" è il momento attuale. La misura di $H_0>0$ è stata la prima verifica sperimentale dell'espansione dell'universo. Il valore osservato è $H_0\approx\SI[per-mode=fraction]{70}{\kilo\metre\per\second\per\mega\parsec}$. Quindi una galassia a distanza di $\SI{1}{\mega\parsec}$ ha uno $zc\approx\SI[per-mode=symbol]{70}{\kilo\metre\per\second}$. $zc$ può anche essere interpretata come una "velocità di allontanamento" che causa il redshift per effetto Doppler. Scritto come $H_0=\frac{c}{L_{H_0}}$ dove $L_{H_0}$ è una distanza abbiamo $L_{H_0}=\SI{4e3}{\mega\parsec}$. Scritto come $H_0=\frac{1}{T_{H_0}}$ dove $T_{H_0}$ è un tempo abbiamo $T_{H_0}\approx\SI{e10}{anni}$.


\section{Dinamica dell'universo}
Vediamo ora come calcolare la "dinamica" dell'universo, cioè come determinare il fattore di scala $a(t)$ dalle equazioni di Einstein. Prendiamo la metrica nella forma \ref{13.1}
\[\dd{s}^2=a^2(\eta)\qty(\dd{\eta}^2-\frac{\dd{r}^2}{1-kr^2}-r^2\qty\big(\dd{\theta}^2+\sin^2\theta\dd{\vp}^2))\]
\[\qty[g_{\mu\nu}]=a^2(\eta)\mqty(\dmat{1,-\frac{1}{1-kr^2},-r^2,-r^2\sin^2\theta})\]
\footnote{\[\qty[g^{\mu\nu}]=\frac{1}{a^2(\eta)}\mqty(\dmat{1,-\qty(1-kr^2),-\frac{1}{r^2},-\frac{1}{r^2\sin^2\theta}})\]}con $\xm=(\eta,r,\theta,\vp)$.

\subsubsection{Connessione}
Calcoliamo la connessione:
\begin{align*}
	&\Gamma^\eta_{\eta\eta}=\frac{1}{2}g^{\eta\eta}\p_\eta g_{\eta\eta}=\frac{\p_\eta a}{a} & &\Gamma^i_{\eta\eta}=0\\
	&\Gamma^\eta_{\eta i}=\Gamma^\eta_{i\eta}=0 & &\Gamma^\eta_{ij}=-\frac{1}{2}g^{\eta\eta}\p_{\eta}g_{ij}=\frac{\p_\eta a}{a}h_{ij}\\
	&\Gamma^i_{j\eta}=\Gamma^i_{\eta j}=\frac{1}{2}g^{ik}\p_\eta g_{kj}=\frac{\p_\eta a}{a}\tensor{\delta}{^i_j} & &\Gamma^i_{jk}=\frac{1}{2}h^{is}\qty\big(\p_jh_{sk}+\p_kh_{sj}-\p_sh_{ij})
\end{align*}
\begin{comment}
\[\Gamma^\eta_{\eta\eta}=\frac{1}{2}g^{\eta\eta}\p_\eta g_{\eta\eta}=\frac{\p_\eta a}{a} \qquad \Gamma^i_{\eta\eta}=\Gamma^\eta_{\eta i}=\Gamma^\eta_{i\eta}=0 \qquad \Gamma^\eta_{ij}=-\frac{1}{2}g^{\eta\eta}\p_{\eta}g_{ij}=\frac{\p_\eta a}{a}h_{ij}\]
\[\Gamma^i_{j\eta}=\Gamma^i_{\eta j}=\frac{1}{2}g^{ik}\p_\eta g_{kj}=\frac{\p_\eta a}{a}\tensor{\delta}{^i_j} \qquad \Gamma^i_{jk}=\frac{1}{2}h^{is}\qty\big(\p_jh_{sk}+\p_kh_{sj}-\p_sh_{ij})\]
\end{comment}
dove abbiamo usato
\[\qty[h_{ij}]=\mqty(\dmat{\frac{1}{1-kr^2},r^2,r^2\sin^2\theta})\]

\subsubsection{Tensore di Ricci e curvatura scalare}
\[R_{\mu\nu}=\p_\rho\Gamma^\rho_{\mu\nu}-\p_\nu\Gamma^\rho_{\mu\rho}+\Gamma^\rho_{\lambda\rho}\Gamma^\lambda_{\mu\nu}-\Gamma^\rho_{\lambda\nu}\Gamma^\lambda_{\mu\rho}\]
\begin{align*}
	&R_{\eta\eta}=%...=
	-3\p_\eta\qty(\frac{\p_\eta a}{a})\\
	&R_{\eta i}=%...=
	0 \qquad \text{(per isotropia)}\\
	&R_{ij}=%...=
	h_{ij}\qty(2k+\p_\eta\qty(\frac{\p_\eta a}{a})+2\qty(\frac{\p_\eta a}{a})^2)
\end{align*}
%dove $\underbar{R}_{ij}$ è quello calcolato con $h_{ij}$, $\underbar{R}_{ij}=2kh_{ij}$.
Ricaviamo la curvatura scalare (con $R=g^{\eta\eta}R_{\eta\eta}+g^{ij}R_{ij}$ e $g^{ij}h_{ij}=-\frac{3}{a^2}$):
\begin{equation}\label{13.2}
	R=-\frac{3}{a^2}\qty(2k+2\p_\eta\qty(\frac{\p_\eta a}{a})+2\qty(\frac{\p_\eta a}{a})^2)
\end{equation}

\subsubsection{Tensore ed equazioni di Einstein}
Il tensore di Einstein è quindi:
\begin{align*}
	&G_{\eta\eta}=%R_{\eta\eta}-\frac{1}{2}g_{\eta\eta}R=...=
	3\qty(k+\qty(\frac{\p_\eta a}{a}^2))\\
	&G_{\eta i}=G_{i\eta}=0\\
	&G_{ij}=%R_{ij}-\frac{1}{2}g_{ij}R=...=
	-h_{ij}\qty(k+2\p_\eta\qty(\frac{\p_\eta a}{a})+\qty(\frac{\p_\eta a}{a})^2)
\end{align*}
Ora dobbiamo scrivere il tensore $T_{\mu\nu}$. Un fluido che rispetti le simmetrie del problema deve essere fermo rispetto al sistema di riferimento $r,\theta,\vp$ quindi
\[T_{\mu\nu}=(\rho+p)u_\mu u_\nu-pg_{\mu\nu} \qquad \text{con} \quad u_\mu=(\sqrt{g_{00}},0,0,0)\]
Inoltre $\rho$ e $p$ dipendono solo da $\eta$. Quindi ($g_{ij}=-a^2h_{ij}$)
\[T_{\eta\eta}=a^2\rho \qquad T_{ij}=a^2ph_{ij}\]
le equazioni di Einstein sono essenzialmente due
\begin{equation}\label{13.7}
	\begin{dcases}
	G_{\eta\eta}=\dfrac{8\pi G_N}{c^4}T_{\eta\eta}\\G_{ij}=\dfrac{8\pi G_N}{c^4}T_{ij}
	\end{dcases}\implies
	\begin{dcases}
	3\qty(k+\qty(\frac{\p_\eta a}{a})^2)=\frac{8\pi G_N}{c^4}a^2\rho\\
	-k-2\p_\eta\qty(\frac{\p_\eta a}{a})-\qty(\frac{\p_\eta a}{a})^2=\frac{8\pi G_N}{c^4}a^2p
	\end{dcases}
\end{equation}
\begin{comment}
\begin{align*}
	&G_{\eta\eta}=\frac{8\pi G_N}{c^4}T_{\eta\eta} \implies 3\qty(k+\qty(\frac{\p_\eta a}{a})^2)=\frac{8\pi G_N}{c^4}a^2\rho\\
	&G_{ij}=\frac{8\pi G_N}{c^4}T_{ij} \implies -k-2\p_\eta\qty(\frac{\p_\eta a}{a})-\qty(\frac{\p_\eta a}{a})^2=\frac{8\pi G_N}{c^4}a^2p
\end{align*}
\end{comment}
Dalla conservazione dell'energia e dell'impulso, $D_\mu T^{\mu\nu}=0$, abbiamo per $\nu=\eta$
\[\p_\mu T^{\mu\eta}+\Gamma^\mu_{\lambda\mu}T^{\lambda\eta}+\Gamma^\eta_{\lambda\mu}T^{\mu\lambda}=0\]
sommando su $\mu$ e ricordando $T^{i\eta}=0$ (quindi $\p_iT^{i\eta}=0$) ricaviamo
\begin{align*}
	&\p_\eta T^{\eta\eta}+\Gamma^\eta_{\eta\eta}T^{\eta\eta}+\Gamma^i_{\eta i}T^{\eta\eta}+\Gamma^\eta_{\eta\eta}T^\eta_{\eta\eta}+\Gamma^\eta_{ij}T^{ij}=0 \qquad T^{\eta\eta}=\frac{\rho}{a^2} \quad T^{ij}=\frac{ph^{ij}}{a^2}\\
	&\p_\eta\qty(\frac{\rho}{a^2})+3\frac{\rho}{a^2}\frac{\p_\eta a}{a}+3\frac{p}{a^2}\frac{\p_\eta a}{a}=0\\
	&\p_\eta\rho+3\frac{\p_\eta a}{a}(\rho+p)=0 \qquad \frac{\p_\eta\rho}{\rho+p}+3\frac{\p_\eta a}{a}=0 \quad \text{(oppure $\p_\eta\qty(a^3\rho)=-p\p_\eta(a^3)$)}
\end{align*}


\subsubsection{Equazione di stato}
Prendiamo ora un'equazione di stato del tipo $p=w\rho$ con:
\begin{itemize}
	%\itemsep-0.1em
	\item $w=0$ per materia non relativistica
	\item $w=1/3$ per materia relativistica
	\item $w=-1$ una costante cosmologica
\end{itemize}
\begin{equation}\label{13.8}
	\frac{1}{1+w}\frac{\p_\eta\rho}{\rho}=-3\frac{\p_\eta a}{a} \qquad \text{quindi} \quad \rho=\rho_0\qty(\frac{a_0}{a})^{3(1+w)}
\end{equation}
%\[\frac{1}{1+w}\frac{\p_\eta\rho}{\rho}=-3\frac{\p_\eta a}{a} \qquad \text{quindi} \quad \rho=\alpha a^\beta\]
%\[\frac{1}{1+w}\beta=-3\implies\beta=-3(1+w) \qquad \text{quindi} \quad \rho=\rho_0\qty(\frac{a_0}{a})^{3(1+w)}\]
Per materia non relativistica $E=\rho V_\text{spaziale}$ rimane costante e infatti $\rho\propto1/a^3$. Per materia relativistica $\rho\propto\frac{1}{a^4}$, $\frac{1}{a^3}$ dal fattore di volume e $\frac{1}{a}$ dal redshift gravitazionale. Per una costante cosmologica $\rho=\text{costante}$. Mettendo il risultato in $G_{\eta\eta}=\frac{8\pi G_N}{c^4}T_{\eta\eta}$ otteniamo
\begin{equation}\label{13.3}
	3\qty(k+\qty(\frac{\p_\eta a}{a})^2)=\frac{8\pi G_N}{c^4}a^2\rho_0\qty(\frac{a_0}{a})^{3(1+w)}
\end{equation}
Studiamo ora le soluzioni della \ref{13.3} in alcuni casi limite.


\subsection{Universo dominato della materia} %Materia non relativistica
Cominciamo a studiare le soluzioni per $w=0$. Questa si chiama anche universo dominato della materia (non relativistica). La \ref{13.3} si scrive come:
\[\p_\eta a=\sqrt{\xi a-ka^2} \qquad \text{con} \quad \xi=\frac{8\pi G_N}{3c^4}\rho_0a_0^3\]
%\subsection{Materia non relativistica}
Per piccolo $a$ scompare la dipendenza dalla curvatura spaziale $k$: $\p_\eta a=\sqrt{\xi a}$ la cui soluzione è $a\simeq\frac{\xi}{4}\qty(\eta+\text{cost})^2$. La costante d'integrazione si può scegliere prendendo $a\ra0$ per $\eta\ra0$. La soluzione generale è
\[a(\eta)=\begin{dcases*}
\frac{\xi}{2}\qty\big(1-\cos\eta)	& per $k=+1$ universo chiuso\\
\frac{\xi}{4}\eta^2		& per $k=0$ \ \ universo piatto\\
\frac{\xi}{2}\qty\big(-1+\cosh\eta) & per $k=-1$ universo aperto
\end{dcases*}\]
\begin{figure}[h!]
	\vspace{-10pt}
	\centering
	\includegraphics[width=0.7\textwidth]{./immagini/materia.png}
	\vspace{-10pt}
\end{figure}

\noindent Per $k=0,-1$ abbiamo anche soluzioni in cui $a(\eta)$ è in perenne contrazione, queste però non sono fisicamente interessanti visto che conosciamo $H(\eta_0)>0$.


\subsection{Universo dominato dalla radiazione} %Materia relativistica
Vediamo ora il caso di universo dominato dalla radiazione (materia relativistica). L'equazione da risolvere è la \ref{13.3} con $w=1/3$:
\[\p_\eta a=\sqrt{\omega-ka^2} \qquad \text{con} \quad \omega=\frac{8\pi G_N}{3c^4}\rho_0a_0^4=\xi a_0\]
Per $a$ piccolo la soluzione è $a\simeq\sqrt{\omega}\eta$. In generale
\[a(\eta)=\begin{cases*}
\sqrt{\omega}\sin\eta	& per $k=1$ \ \ universo chiuso\\
\sqrt{\omega}\eta		& per $k=0$ \ \ universo piatto\\
\sqrt{\omega}\sinh\eta	& per $k=-1$ universo aperto
\end{cases*}\]
\begin{figure}[h]
	\vspace{-10pt}
	\centering
	\includegraphics[width=0.7\textwidth]{./immagini/radiazione.png}
	\vspace{-10pt}
\end{figure}

\subsubsection{Singolarità}
Vicino a $\eta\simeq0$ abbiamo, sia nel caso di materia che di radiazione, una singolarità intrinseca, $R\ra\infty$. Ad esempio per la radiazione\footnote{Facciamo il limite $\eta\ra0$: $\sin\eta\sim\sinh\eta\sim\eta$, quindi $a\sim\sqrt{\omega}\eta$. Per l'universo dominato da materia, con l'espansione attorno a $\eta=0$ al secondo ordine in $\eta$, avremmo $a\sim\frac{\xi}{4}\eta^2$.}
\[R=-\frac{6}{a^2}\qty(k+\p_\eta\qty(\frac{\p_\eta a}{a})+\qty(\frac{\p_\eta a}{a})^2)\simeq-\frac{6}{\omega\eta^2}\qty(k+\frac{2}{\eta^2})\simeq-\frac{12}{\omega\eta^4}\]
quindi diverge per $\eta\ra0^+$. Questa è chiamata anche \textit{singolarità del Big Bang}.

\subsubsection{Big Crunch}
Nel caso di un universo chiuso $k=+1$ abbiamo anche un \textit{Big Crunch} a $\eta=2\pi$ o $\pi$ mentre per un universo piatto o aperto, $k=0,-1$, l'espansione continua per sempre. Nel caso di universo chiuso un raggio di luce per fare un giro completo attorno ad un cerchio massimo di $S^3$ deve andare da $\chi=0$ a $\chi=\pi$ e poi tornare indietro, quindi in totale $\chi=2\pi$. Ha quindi bisogno di un $\var{\eta}=\var{\chi}=2\pi$. L'unico caso fra i precedenti in cui questo è possibile è nell'universo dominato dalla materia, se il raggio parte al momento del Big Bang $\eta=0$ ritorna nel punto di partenza esattamente al Big Crunch $\eta=2\pi$.


\section{Orizzonte}
Introduciamo ora il concetto di orizzonte in cosmologia. Se prendiamo un osservatore ad un certo $\eta_0$ e $\chi=0$, esso può osservare raggi luminosi che provengono al massimo da $\chi_\text{max}=\eta_0$. I raggi da $\chi=\chi_\text{max}$ sono quelli che partono al momento del Big Bang e $\eta_0$ è il $\var{\eta}=\eta_0-\eta_\text{BB}$ (con BB che sta per Big Bang) dove abbiamo preso per convenzione $\eta_\text{BB}=0$. Questa nozione di orizzonte dipende ovviamente dall'osservatore, e dal suo tempo conforme $\eta_0$; si chiama anche più precisamente \textit{orizzonte di particella}. Gli eventi con $\chi>\chi_\text{max}$, cioè fuori dall'orizzonte, non solo non possono essere osservati ma non posso nemmeno avere un'influenza causale con ciò che succede all'osservatore fino a $\eta_0$.

Scriviamo le soluzioni in funzione del tempo $t$.
\begin{itemize}
	\item Per $k=0$ e $w=0$, $a(\eta)=\frac{\xi}{4}\eta^2$, $c\dd{t}=\frac{\xi}{4}\eta^2\dd{\eta}$ quindi\footnote{con il solito abuso di notazione indichiamo con la stessa lettera $a$ sia la funzione dipendente da $\eta$, $a(\eta)$, che quella ottenuta con il cambio di variabili dipendente da $t$, $a(t)$}
	\[t=\frac{\xi}{12c}\eta^3 \qquad a(t)=\frac{12^{2/3}\xi^{1/3}c^{2/3}}{4}t^{2/3}\]
	\item Per $k=0$ e $w=\frac{1}{3}$, $a(\eta)=\sqrt{\omega}\eta$, $c\dd{t}=\sqrt{\omega}\eta\dd{\eta}$ quindi
	\[t=\frac{\sqrt{\omega}}{2c}\eta^2 \qquad a(t)=\omega^{1/4}\sqrt{2c}t^{1/2}\]
	Vediamo che in entrambi i casi $\ddot{a}<0$, cioè l'universo si espande ma con accelerazione negativa.
	
	Dalle \ref{13.7}, possiamo scrivere le equazioni di Einstein come
	\[\frac{k}{a^2}+\frac{1}{a^2}\qty(\frac{\p_\eta a}{a})^2=\frac{8\pi G_N}{3c^4}\rho \qquad \text{e} \qquad \frac{2}{a^2}\p_\eta\qty(\frac{\p_\eta a}{a})=-\frac{8\pi G_N}{3c^2}\qty\big(\rho+3p)\]
	usando il tempo $t$, con $\p_\eta=\frac{a}{c}\p_t$, abbiamo
	\begin{equation}\label{13.10}
	\begin{dcases}
	\frac{k}{a^2}+\qty(\frac{\dot{a}}{ca})^2=\frac{8\pi G_N}{3c^4}\rho\\
	\frac{2}{c^2}\frac{\ddot{a}}{a}=-\frac{8\pi G_N}{3c^2}\qty\big(\rho+3p)
	\end{dcases}
	\end{equation}
	quindi il segno di $\ddot{a}$ è dato da quello di $-(\rho+3p)=-\rho(1+3w)$.
	\item Per $w>-\frac{1}{3}$ abbiamo sempre $\ddot{a}<0$, indipendentemente da $k$. (L'equazione $\p_t(a^3\rho)+p\p_ta^3=0$, cioè $\dot{\rho}+3(p+\rho)\frac{\dot{a}}{a}=0$ è contenuta nelle due sopra: $\frac{8\pi G_N}{3c^4}\qty(2a\dot{a}\rho+a^2\dot{\rho})=\frac{2\dot{a}\ddot{a}}{c^2}=-a\dot{a}\frac{8\pi G_N}{3c^4}(\rho+3p)\implies\dot{\rho}=-3\frac{\dot{a}}{a}(\rho+p)$.)
\end{itemize}

\subsubsection{Età dell'universo}
Calcoliamo l'età dell'universo, cioè il tempo $t$ trascorso tra il Big Bang e il tempo attuale $t_0$, in funzione della costante di Hubble $H_0$. Per $k=0$, $w=0$, $a(t)\propto t^{2/3}$ quindi dalla \ref{13.5} $t_0=\frac{2}{3H_0}$ mentre per $k=0$, $w=\frac{1}{3}$, $a(t)\propto t^{1/2}$ quindi $t_0=\frac{1}{2H_0}$. In genere
\[t_0=\frac{1}{H_0}\]
è una stima (in eccesso se $\ddot{a}<0$) dell'età dell'universo.


\section{Composizione dell'universo}
Vediamo ora altre informazioni osservate circa la composizione di $\rho$ nell'universo attuale. Possiamo riscrivere\footnote{moltiplicando per $\frac{c^2}{H_0^2}$ la prima delle \ref{13.10}} la prima equazione di Einstein come
\[\frac{1}{H_0^2}\qty(\frac{\dot{a}}{a})^2=\frac{8\pi G_N}{3c^2H_0^2}\rho-\frac{kc^2}{a^2H_0^2}\]
che\footnote{dalla definizione \ref{13.5} $H_0=\frac{\dot{a}(t_0)}{a(t_0)}$} calcolata al tempo attuale $t_0$ è: ($\rho_c$ è chiamata \textit{densità critica})
\[1=\frac{\rho}{\rho_c}-\frac{kc^2}{a_0^2H_0^2} \qquad \text{dove} \quad \boxed{\rho_c\equiv\frac{3c^2H_0^2}{8\pi G_N}}\simeq10^{-26}\qty(\frac{H_0}{\SI[per-mode=fraction]{70}{\kilo\metre\per\second\per\mega\parsec}})^2c^2\si[per-mode=fraction]{\kilogram\per\cubic\metre}\]
Misurare $\rho$ non è semplice, si possono dare delle stime per difetto considerando le componenti che "vediamo".

La materia "luminosa" è anche chiamata \textit{materia barionica} perché la maggior parte della massa viene dai protoni e neutroni nei nuclei atomici. Con vari metodi osservativi, diretti e indiretti, si ottiene $\rho_{b}\simeq4\times10^{-28}c^2\si[per-mode=symbol]{kg/m^3}$ quindi $\rho_b/\rho_c\simeq0.04$. Quindi $\rho_b$ è una piccola parte, il resto è in altre forme di energia oppure nella componente data dalla curvatura spaziale $-\frac{kc^2}{a_0^2H_0^2}$.

Un'altra componente di $\rho$ che osserviamo è sotto forma di radiazione elettromagnetica. L'universo è permeato da un fondo di radiazione praticamente isotropo con distribuzione di corpo nero ad una temperatura $T_\text{cmb}$ dove "cmb" sta per "cosmic microwave background". I fotoni di questa radiazione hanno energia $h\nu\simeq\SI{6.6e-4}{\electronvolt}$ e lunghezza d'onda nelle micro-onde $\lambda\simeq\SI{0.5}{\centi\metre}$. La densità di energia è
\[\rho_\text{cmb}\simeq2\frac{\pi^2}{30\hbar^3c^3}\qty(k_BT)^4\simeq c^2\SI[per-mode=symbol]{4.4e-31}{\kilogram\per\cubic\metre} \qquad \text{quindi} \quad \frac{\rho_\text{cmb}}{\rho_b}\simeq10^{-3}\]
La cmb fornisce un contributo di energia trascurabile rispetto alla materia barionica, circa $1/1000$. Questo però non è valido sempre, ma solo calcolato al momento attuale $t_0$. Le componenti di materia (non relativistica) e radiazione scalano infatti in maniera diversa con $a$, dalla \ref{13.8} abbiamo che $\rho_b\propto\frac{1}{a^3}$ mentre $\rho_\text{cmb}\propto\frac{1}{a^4}$. Quindi ad un certo momento nel passato abbiamo equivalenza fra il contenuto energetico della radiazione e della materia\footnote{$t_\text{eq}$ è definito in modo che $\frac{\rho_\text{rad}(t_\text{eq})}{\rho_\text{mat}(t_\text{eq})}=1$. Possiamo riscrivere la \ref{13.8} come $\rho=\rho_\text{eq}\qty(\frac{a_\text{eq}}{a})^{3(1+w)}$, quindi $\rho(t_0)=\rho_\text{eq}\qty(\frac{a_\text{eq}}{a_0})^{3(1+w)}$.}
\[\frac{a(t_\text{eq})}{a(t_0)}=\frac{\rho_\text{cmb}(t_0)}{\rho_\text{mat}(t_0)}\leq\frac{\rho_\text{cmb}(t_0)}{\rho_b(t_0)}\simeq10^{-3}\]
dove $t_\text{eq}$ è chiamato \textit{tempo di equivalenza}. Per $t<t_\text{eq}$ l'universo è dominato dalla radiazione mentre per $t>t_\text{eq}$ è dominato dalla materia. Abbiamo chiamato $\rho_{\text{mat},0}$ il contenuto totale di materia adesso. $\rho_\text{mat}\geq\rho_b$ dove ammettiamo la possibilità di materia non barionica, chiamata anche \textit{materia oscura}. Per via del redshift cosmologico la cmb al momento dell'equivalenza ha una temperatura maggiore
\[k_BT(t_\text{eq})=h\nu(t_\text{eq})=h\nu(t_0)\frac{a(t_0)}{a(t_\text{eq})}=k_BT(t_0)\frac{a(t_0)}{a(t_\text{eq})}\simeq k_BT(t_0)10^3\simeq\SI{1}{\electronvolt}\]
nota che questo è l'ordine di grandezza delle transizioni atomiche.

Un altro momento importante per la cmb è il tempo $t_\text{rec}$ detto \textit{tempo della ricombinazione}. Questo sarebbe il tempo in cui la radiazione cmb che osserviamo ora viene effettivamente emessa, cioè l'ultimo momento in cui c'era materia carica in equilibrio termico con la radiazione. Questo è anche chiamato \textit{tempo di ultimo scattering}. L'universo diventa effettivamente "trasparente" alla radiazione quando protoni ed elettroni si ricombinano formando un atomo neutro di idrogeno. Questo succede circa quando $k_BT\simeq\SI{10}{\electronvolt}$, quindi $t_\text{rec}$ non è molto diverso da $t_\text{eq}$.


\subsection{Problema dell'orizzonte}
Facciamo l'approssimazione $\eta_\text{rec}\approx\eta_\text{eq}$ e prendiamo per semplicità $k=0$. L'universo è quindi risolto in tempo conforme da
\[\begin{dcases*}
a(\eta)=\frac{\xi}{4}\eta^2 & per $\eta\geq\eta_\text{eq}$\\
a(\eta)=\frac{\xi}{2}\eta_\text{eq}\qty(\eta-\frac{1}{2}\eta_\text{eq}) & per $\frac{1}{2}\eta_\text{eq}\leq\eta\leq\eta_\text{eq}$
\end{dcases*}\]
abbiamo risolto imponendo $a$ e $\p_\eta a$ continue al passaggio $\eta_\text{eq}$ fra le due soluzioni. Quindi nel tempo conforme il Big Bang avviene a $\eta=\frac{1}{2}\eta_\text{eq}$.
\[\frac{\eta_\text{eq}}{\eta_0}\approx\frac{\eta_\text{rec}}{\eta_0}\simeq\qty(\frac{a(\eta_\text{rec})}{a(\eta_0)})^{1/2}\simeq\qty(\frac{T_\text{cmb}(t_0)}{T_\text{cmb}(t_\text{rec})})^{1/2}\simeq\numrange[range-phrase =\olddiv]{e-1}{e-2}\]
%DISEGNO
Quindi l'orizzonte della cmb al momento dell'ultimo scattering è molto più piccolo dell'orizzonte attuale. Se noi osserviamo la cmb in due angoli diametralmente opposti nel cielo stiamo osservando eventi che non possono avere un passato causale comune. Eppure la cmb è estremamente omogenea a tutti gli angoli con un $\frac{\var{T}}{T}\approx10^{-5}$. Questo è il \textit{problema dell'orizzonte}.