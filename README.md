# Appunti di GR

[![build status](https://gitlab.com/a.falco/appunti-di-relativita-generale/badges/master/pipeline.svg)](https://gitlab.com/a.falco/appunti-di-relativita-generale/-/jobs/artifacts/master/raw/AppuntiGR.pdf?job=build)

Appunti di Relatività Generale del corso tenuto all'Università di Pisa dal Prof. Stefano Bolognesi durante l'a.a. 2020-2021. L'ultimo pdf è scaricabile [qui](https://gitlab.com/a.falco/appunti-di-relativita-generale/-/jobs/artifacts/master/raw/AppuntiGR.pdf?job=build).
