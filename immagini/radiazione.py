from matplotlib import pyplot as plt
import numpy as np

sqrt_omega=1
eta = np.linspace(0,np.pi,4000)
k1 = sqrt_omega*np.sin(eta)
k2 = sqrt_omega*eta
k3 = sqrt_omega*np.sinh(eta)
plt.plot(eta,k1)
plt.plot(eta,k2)
plt.plot(eta,k3)
#plt.xlim(0,3)
plt.ylim(-0.2,3.5)
plt.xlabel('$\eta$')
plt.ylabel('$a(\eta)$')
plt.grid()
plt.show()
