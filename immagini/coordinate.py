#!/usr/bin/env python3
import numpy as np 
import matplotlib.pyplot as plt
from frecce import add_arrow    #funzione per fare le frecce

rS=1
r = np.linspace(0,5,10000)

for i in range (-10,10,1):
    x0p = r + rS*np.log(r-rS) + i
    x0m = - r - rS*np.log(r-rS) + i
    linep = plt.plot(r, x0p, color='royalblue')[0]
    linem = plt.plot(r, x0m, color='blueviolet')[0]     #color='blueviolet' 'slateblue'
    add_arrow(linep, position=3.4, direction='right', size=15, color=None, arrsty='fancy')
    add_arrow(linem, position=2.1, direction='left', size=15, color=None, arrsty='fancy')
plt.xlim(0.5,5)
plt.ylim(-5,5)
plt.xlabel('r')
plt.ylabel('$x_0$')
plt.grid()
plt.show()
